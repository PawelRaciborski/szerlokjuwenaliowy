package com.juwenalia.ws;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.juwenalia.utilities.Constants.UserTypes;
import com.juwenalia.utilities.SecurityUtils;
import com.juwenalia.ws.models.ActivatedResponse;
import com.juwenalia.ws.models.AddCodeRequest;
import com.juwenalia.ws.models.AppStateRequest;
import com.juwenalia.ws.models.AppStateResponse;
import com.juwenalia.ws.models.BaseRequestJson;
import com.juwenalia.ws.models.BaseResponseJson;
import com.juwenalia.ws.models.GrandPrizeRequest;
import com.juwenalia.ws.models.NormalLoginRequest;
import com.juwenalia.ws.models.PrizesResponse;
import com.juwenalia.ws.models.RankingResponse;
import com.juwenalia.ws.models.RegistrationLoginResponse;
import com.juwenalia.ws.models.RegistrationRequest;

public class WebServiceProvider {
	private final static String BASE_URL = "http://szerlok.herokuapp.com/api/v1";
	private final static String WS_REGISTRATION = "/user/register";
	private final static String WS_REGISTRATION_FB = "/user/facebook";
	private final static String WS_LOGIN = "/user/login";
	private static final String WS_ACTIVATED = "/user/active";
	private static final String WS_GET_PRIZES = "/prizes";
	private static final String WS_APP_STATE = "/appstate";
	private static final String WS_ADD_CODE = "/user/code";
	private static final String WS_RANKING = "/ranking?user_type=";
	private static final String WS_GRANT_PRIZE = "/prizes/grant";

	private static final String IMAGE_BASE_URL = "https://dl.dropboxusercontent.com/u/8192112/";

	private final static int TIMEOUT = 30000;

	private static WebServiceProvider instance = null;
	private DefaultHttpClient httpClient;
	private Gson gson;

	/* SINGLETON */

	protected WebServiceProvider() {
		this.httpClient = new DefaultHttpClient();
		gson = new Gson();
	}

	public static WebServiceProvider getInstance() {
		if (instance == null) {
			instance = new WebServiceProvider();
		}
		return instance;
	}

	public String testConnection() throws ClientProtocolException, IOException {
		HttpGet httpGet = new HttpGet(BASE_URL);
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParameters, TIMEOUT);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpResponse response = httpClient.execute(httpGet);
		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity()
				.getContent(), "UTF-8"));
		return reader.readLine();
	}

	/* SYSTEM WEBSERVICES */

	public RegistrationLoginResponse callRegistrationWebsercive(String username, String email,
			String password, String facebookId, UserTypes type) throws ClientProtocolException,
			UnsupportedEncodingException, IOException {
		String hPassword = SecurityUtils.hashText(password);

		RegistrationRequest request = new RegistrationRequest();
		request.userName = username;
		request.email = email;
		request.password = hPassword;
		request.userType = type.getValue();
		request.facebookId = facebookId;
		String str;
		if (type == UserTypes.NORMAL) {
			str = POST(BASE_URL + WS_REGISTRATION, request);
		} else {
			// str =
			// "{\"code\":0,\"message\":\"Hello facebook user\",\"payload\":{\"user_type\":2,\"user_name\":\"pawel.dattebayo\",\"activated\":true,\"user_id\":32}}";
			str = POST(BASE_URL + WS_REGISTRATION_FB, request);
		}
		RegistrationLoginResponse response = gson.fromJson(str, RegistrationLoginResponse.class);
		return response;
	}

	public RegistrationLoginResponse callNormalLoginWebsercive(String email, String password)
			throws ClientProtocolException, UnsupportedEncodingException, IOException {
		String hPassword = SecurityUtils.hashText(password);

		NormalLoginRequest request = new NormalLoginRequest();
		request.email = email;
		request.password = hPassword;
		String str = POST(BASE_URL + WS_LOGIN, request);
		RegistrationLoginResponse response = gson.fromJson(str, RegistrationLoginResponse.class);
		return response;
	}

	public Object callCheckActivatedWebservice(String userId) throws ClientProtocolException,
			UnsupportedEncodingException, IOException {
		BaseRequestJson request = new BaseRequestJson();
		request.userId = userId;
		String str = POST(BASE_URL + WS_ACTIVATED, request);
		ActivatedResponse response = gson.fromJson(str, ActivatedResponse.class);
		return response;
	}

	public Object callGetPrizesWebsercive() throws ClientProtocolException,
			UnsupportedEncodingException, IOException {
		String str = GET(BASE_URL + WS_GET_PRIZES);
		PrizesResponse response = gson.fromJson(str, PrizesResponse.class);
		return response;
	}

	public Object callAppState(String userId, LinkedList<Long> clues, String timestamp)
			throws ClientProtocolException, UnsupportedEncodingException, IOException {
		AppStateRequest request = new AppStateRequest(userId, clues, timestamp);
		String str = POST(BASE_URL + WS_APP_STATE, request);
		AppStateResponse response = gson.fromJson(str, AppStateResponse.class);
		return response;
	}

	public Object callAddCode(String userId, Long codeId) throws ClientProtocolException,
			UnsupportedEncodingException, IOException {
		AddCodeRequest request = new AddCodeRequest();
		request.userId = userId;
		request.codeId = codeId.toString();
		String str = POST(BASE_URL + WS_ADD_CODE, request);
		BaseResponseJson response = gson.fromJson(str, BaseResponseJson.class);
		return response;
	}

	public Object callGetRankingWebsercive(UserTypes type) throws ClientProtocolException,
			UnsupportedEncodingException, IOException {

		String str = GET(BASE_URL + WS_RANKING + type.getValue());
		RankingResponse response = gson.fromJson(str, RankingResponse.class);
		return response;
	}

	public Object callGrantPrize(String userId, LinkedList<Long> codes, Long codeId)
			throws Exception {
		GrandPrizeRequest request = new GrandPrizeRequest(userId, codes, codeId);
		String str = POST(BASE_URL + WS_GRANT_PRIZE, request);
		if (!str.contains("code")) {
			throw new Exception("Niew�a�ciwa odpowied� serwera");
		}
		BaseResponseJson response = gson.fromJson(str, BaseResponseJson.class);
		return response;
	}

	/* DOWNLOADNING IMAGES */

	public byte[] downloadImage(Context context, String link) throws IOException {
		URL url = new URL(link);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoInput(true);
		connection.connect();
		InputStream input = connection.getInputStream();
		BufferedInputStream bis = new BufferedInputStream(input, 128);
		ByteArrayBuffer baf = new ByteArrayBuffer(128);
		// get the bytes one by one
		int current = 0;
		while ((current = bis.read()) != -1) {
			baf.append((byte) current);
		}
		return baf.toByteArray();
	}

	/* NETWORK UTILITIES */

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	private String POST(String url, Object request) throws ClientProtocolException, IOException,
			UnsupportedEncodingException {
		String json = gson.toJson(request);

		// ENCRYPTION TEST
		//String a = SecurityUtils.encode(json);
		//String b = SecurityUtils.decode(a);
		// String enc = SecurityUtils.encryptString(json);
		// String dec = SecurityUtils.dencryptString(enc);

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		StringEntity se = new StringEntity(json);
		httpPost.setEntity(se);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		HttpResponse httpResponse = httpclient.execute(httpPost);
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity()
				.getContent(), "UTF-8"));
		String str = reader.readLine();
		return str;

	}

	private String GET(String url) throws ClientProtocolException, IOException,
			UnsupportedEncodingException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("Accept", "application/json");
		httpGet.setHeader("Content-type", "application/json");
		HttpResponse httpResponse = httpclient.execute(httpGet);
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity()
				.getContent(), "UTF-8"));
		String str = reader.readLine();
		return str;

	}
}
