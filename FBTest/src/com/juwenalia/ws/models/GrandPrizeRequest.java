package com.juwenalia.ws.models;

import java.util.LinkedList;

import com.google.gson.annotations.SerializedName;

public class GrandPrizeRequest extends BaseRequestJson {
	public LinkedList<Long> qrcodes;
	@SerializedName("prize_id")
	public String prizeId;
	public GrandPrizeRequest(String userId,LinkedList<Long> codes, Long prizeId) {
		this.userId = userId;
		this.qrcodes = codes;
		this.prizeId = prizeId.toString();
	}
	
}
