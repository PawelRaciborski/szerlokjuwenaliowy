package com.juwenalia.ws.models;

import java.util.LinkedList;

public class AppStateRequest extends BaseRequestJson {
	public LinkedList<Long> codes;
	public String timestamp;

	public AppStateRequest(String userId, LinkedList<Long> clues, String timestamp) {
		this.userId = userId;
		this.codes = clues;
		this.timestamp = timestamp;
	}
}
