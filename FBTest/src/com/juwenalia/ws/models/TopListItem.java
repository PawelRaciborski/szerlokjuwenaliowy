package com.juwenalia.ws.models;

public class TopListItem {

	// public int position;
	public String username;
	public int points;

	public TopListItem(int position, String username, int points) {
		// this.position = position;
		this.username = username;
		this.points = points;
	}

}
