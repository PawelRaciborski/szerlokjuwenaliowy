package com.juwenalia.ws.models;

import com.google.gson.annotations.SerializedName;

public class RegistrationRequest {

	@SerializedName("user_name")
	public String userName;

	public String email;

	public String password;

	@SerializedName("user_type")
	public int userType;

	@SerializedName("facebook_id")
	public String facebookId;
}
