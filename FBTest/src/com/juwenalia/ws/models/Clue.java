package com.juwenalia.ws.models;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

public class Clue {
	public int points;
	@SerializedName("used_points")
	public int usedPoints;
	public String description;
	public double latitude;
	public double longitude;
	public long id;
	public boolean available;
	public String hash;

	public Clue(String description, double latitude, double longitude, int points,
			boolean available, String hash) {
		this.id = -1;
		this.points = points;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		this.available = available;
		this.hash = hash;
	}

	public Clue(long id, String description, double latitude, double longitude, int points,
			int usedPoints, boolean available, String hash) {
		this.id = id;
		this.usedPoints = usedPoints;
		this.points = points;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		this.available = available;
		this.hash = hash;
	}

	public Location getLocation() {
		Location l = new Location("");
		l.setLatitude(latitude);
		l.setLongitude(longitude);
		return l;
	}

	public float getDistance(Location start) {
		if (start != null) {
			return start.distanceTo(getLocation());
		} else {
			return -1.0f;
		}
	}

	public int getIntDistance(Location start) {
		return Math.round(getDistance(start));
	}
}
