package com.juwenalia.ws.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.juwenalia.utilities.Constants.UserTypes;

public class RegistrationLoginResponse extends BaseResponseJson {

	private Payload payload;

	public String getUserId() {
		return payload.userId;
	}

	public String getUsername() {
		return payload.userName;
	}

	public boolean getActivated() {
		return payload.activated;
	}

	public UserTypes getUserType() {
		return UserTypes.intToType(payload.userType);
	}

	private class Payload {
		@SerializedName("user_id")
		public String userId;
		@SerializedName("user_name")
		public String userName;
		public boolean activated;
		@SerializedName("user_type")
		public int userType;
	}

	@Override
	public boolean isResponseValid() {
		if (this.code == 0 && this.payload != null) {
			return true;
		}
		if (this.code != 0 && !TextUtils.isEmpty(this.message)) {
			return true;
		}
		return false;
	}

}
