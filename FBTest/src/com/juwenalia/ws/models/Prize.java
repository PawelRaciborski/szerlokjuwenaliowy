package com.juwenalia.ws.models;

import java.io.ByteArrayInputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Prize {
	public int id;
	public String name;
	public String description;
	public byte[] image;
	public String image_url;
	public int points;
	public boolean available;
	public String address;

	public Prize(String name, String content, int image, int points) {
		this.id = 0;
		this.name = name;
		this.description = content;
		this.image = null;
		this.points = points;
		this.available = true;
		this.address = "Adres: Lorem ipsum";
	}

	public Prize(int id, String name, String content, byte[] image, int points, boolean available,
			String adress) {
		this.id = id;
		this.name = name;
		this.description = content;
		this.image = image;
		this.points = points;
		this.available = available;
		this.address = adress;
	}

	public Bitmap getPrizeBitmap() {
		if (image != null && image.length > 0) {
			ByteArrayInputStream imageStream = new ByteArrayInputStream(image);
			return BitmapFactory.decodeStream(imageStream);
		}
		return null;
	}

}
