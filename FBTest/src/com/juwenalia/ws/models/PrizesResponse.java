package com.juwenalia.ws.models;

import android.text.TextUtils;

public class PrizesResponse extends BaseResponseJson {

	private Prize[] payload;

	@Override
	public boolean isResponseValid() {
		if (this.code == 0 && this.payload != null && payload.length > 0) {
			return true;
		}
		if (this.code != 0 && !TextUtils.isEmpty(this.message)) {
			return true;
		}
		return false;
	}

	public Prize[] getPrizes() {
		return this.payload;
	}

}
