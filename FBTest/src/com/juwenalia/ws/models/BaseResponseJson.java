package com.juwenalia.ws.models;

public class BaseResponseJson {
	public int code;
	public String message;

	public boolean isResponseValid() {
		if (code == 0) {
			return true;
		} else if (code != 0 && message != null) {
			return true;
		} else {
			return false;
		}

	}
}
