package com.juwenalia.ws.models;

import android.text.TextUtils;

public class ActivatedResponse extends BaseResponseJson {

	private Payload payload;

	public boolean getActivated() {
		return this.payload.activated;
	}

	@Override
	public boolean isResponseValid() {
		if (this.code == 0 && this.payload != null) {
			return true;
		}
		if (this.code != 0 && !TextUtils.isEmpty(this.message)) {
			return true;
		}
		return false;
	}

	private class Payload {
		private boolean activated;
	}
}
