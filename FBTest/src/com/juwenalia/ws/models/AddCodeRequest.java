package com.juwenalia.ws.models;

import com.google.gson.annotations.SerializedName;

public class AddCodeRequest extends BaseRequestJson {
	@SerializedName("code_id")
	public String codeId;
}
