package com.juwenalia.ws.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class AppStateResponse extends BaseResponseJson {

	private Payload payload;

	public String getTimestamp() {
		return payload.timestamp;
	}
	public int getPoints() {
		return payload.points;
	}
	public int getUsedPoints() {
		return payload.usedPoints;
	}
	
	public void setTimestamp(String timestamp) {
		this.payload.timestamp = timestamp;
	}

	public String getBaseUrl() {
		return payload.baseUrl;
	}

	public void setBaseUrl(String url) {
		this.payload.baseUrl = url;
	}

	public Clue[] getCodes() {
		return payload.codes;
	}

	public void setCodes(Clue[] codes) {
		this.payload.codes = codes;
	}

	public Prize[] getPrizes() {
		return payload.prizes;
	}

	public void setPrizes(Prize[] prizes) {
		this.payload.prizes = prizes;
	}

	@Override
	public boolean isResponseValid() {
		if (this.code == 0 && this.payload != null) {
			return true;
		}
		if (this.code != 0 && !TextUtils.isEmpty(this.message)) {
			return true;
		}
		return false;
	}

	private class Payload {

		private String timestamp;
		private int points;
		@SerializedName("used_points")
		private int usedPoints;
		private Clue[] codes;
		private Prize[] prizes;
		@SerializedName("img_base_url")
		private String baseUrl;
	}

}
