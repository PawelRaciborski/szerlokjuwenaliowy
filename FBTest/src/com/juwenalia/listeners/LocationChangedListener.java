package com.juwenalia.listeners;

import android.location.Location;

public interface LocationChangedListener {
	public void onLocationChanged(Location location, boolean locationQuality);
}
