package com.juwenalia.listeners;

public interface AsyncTaskListener {
	public void onSuccess(Object result);

	public void onFailure(Object result);

	public void onCancel();
}
