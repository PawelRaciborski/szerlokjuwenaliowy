package com.juwenalia.listeners;

public interface DbChangedListener {
	public enum DbTables {
		Prizes, Clues, Both
	}

	public void dbChanged(DbTables table);
}
