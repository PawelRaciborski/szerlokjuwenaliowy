package com.juwenalia.listeners;

public interface SwitchFragmentListener {
	public void switchFragment(int fragmentId);
}
