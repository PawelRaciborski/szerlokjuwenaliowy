package com.juwenalia.utilities;

import java.io.File;
import java.util.LinkedList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.juwenalia.ws.models.Clue;
import com.juwenalia.ws.models.Prize;

public class DbAdapter {
	private static final String DEBUG_TAG = "SqLitePrizeManager";

	/* DB CONSTANTS */

	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "database.db";
	private static final String DB_PRIZES_TABLE = "prizes";
	private static final String DB_CLUES_TABLE = "clues";

	/* PRIZES CONSTANTS */

	public static final String KEY_ID = "_id";
	public static final String ID_OPTIONS = "INTEGER PRIMARY KEY";
	public static final int ID_COLUMN = 0;

	public static final String KEY_NAME = "name";
	public static final String NAME_OPTIONS = "TEXT NOT NULL";
	public static final int NAME_COLUMN = 1;

	public static final String KEY_DESCRIPTION = "description";
	public static final String DESCRIPTION_OPTIONS = "TEXT NOT NULL";
	public static final int DESCRIPTION_COLUMN = 2;

	public static final String KEY_IMAGE = "image";
	public static final String IMAGE_OPTIONS = "BLOB";
	public static final int IMAGE_COLUMN = 3;

	public static final String KEY_POINTS = "points";
	public static final String POINTS_OPTIONS = "INTEGER DEFAULT 0";
	public static final int POINTS_COLUMN = 4;

	public static final String KEY_AVAILABLE = "available";
	public static final String AVAILABLE_OPTIONS = "INTEGER DEFAULT 0";
	public static final int AVAILABLE_COLUMN = 5;

	public static final String KEY_ADRESS = "adress";
	public static final String ADRESS_OPTIONS = "TEXT NOT NULL";
	public static final int ADRESS_COLUMN = 6;

	/* CLUES CONSTANTS */

	public static final String KEY_CLUE_DESCRIPTION = "description";
	public static final String CLUE_DESCRIPTION_OPTIONS = "TEXT NOT NULL";
	public static final int CLUE_DESCRIPTION_COLUMN = 1;

	public static final String KEY_CLUE_POINTS = "points";
	public static final String CLUE_POINTS_OPTIONS = "INTEGER DEFAULT 0";
	public static final int CLUE_POINTS_COLUMN = 2;

	public static final String KEY_CLUE_USED_POINTS = "usedpoints";
	public static final String CLUE_USED_POINTS_OPTIONS = "INTEGER DEFAULT 0";
	public static final int CLUE_USED_POINTS_COLUMN = 3;

	public static final String KEY_LATITUDE = "latitude";
	public static final String LATITUDE_OPTIONS = "REAL DEFAULT 0.0";
	public static final int LATITUDE_COLUMN = 4;

	public static final String KEY_LONGITUDE = "adress";
	public static final String LONGITUDE_OPTIONS = "REAL DEFAULT 0.0";
	public static final int LONGITUDE_COLUMN = 5;

	public static final String KEY_CLUE_AVAILABLE = "available";
	public static final String CLUE_AVAILABLE_OPTIONS = "INTEGER DEFAULT 0";
	public static final int CLUE_AVAILABLE_COLUMN = 6;

	public static final String KEY_HASH = "hash";
	public static final String HASH_OPTIONS = "TEXT NOT NULL";
	public static final int HASH_COLUMN = 7;

	private static final String DB_CREATE_PRIZES_TABLE = "CREATE TABLE " + DB_PRIZES_TABLE + "( "
			+ KEY_ID + " " + ID_OPTIONS + ", " + KEY_NAME + " " + NAME_OPTIONS + ", "
			+ KEY_DESCRIPTION + " " + DESCRIPTION_OPTIONS + ", " + KEY_IMAGE + " " + IMAGE_OPTIONS
			+ ", " + KEY_POINTS + " " + POINTS_OPTIONS + ", " + KEY_AVAILABLE + " "
			+ AVAILABLE_OPTIONS + ", " + KEY_ADRESS + " " + ADRESS_OPTIONS + ");";
	private static final String DROP_PRIZES_TABLE = "DROP TABLE IF EXISTS " + DB_PRIZES_TABLE;

	private static final String DB_CREATE_CLUES_TABLE = "CREATE TABLE " + DB_CLUES_TABLE + "( "
			+ KEY_ID + " " + ID_OPTIONS + ", " + KEY_CLUE_DESCRIPTION + " "
			+ CLUE_DESCRIPTION_OPTIONS + ", " + KEY_CLUE_POINTS + " " + CLUE_POINTS_OPTIONS + ", "
			+ KEY_CLUE_USED_POINTS + " " + CLUE_USED_POINTS_OPTIONS + ", " + KEY_LATITUDE + " "
			+ LATITUDE_OPTIONS + ", " + KEY_LONGITUDE + " " + LONGITUDE_OPTIONS + ", "
			+ KEY_CLUE_AVAILABLE + " " + AVAILABLE_OPTIONS + ", " + KEY_HASH + " " + HASH_OPTIONS
			+ ");";
	private static final String DROP_CLUES_TABLE = "DROP TABLE IF EXISTS " + DB_CLUES_TABLE;

	private SQLiteDatabase db;
	private Context context;
	private DatabaseHelper dbHelper;

	private static class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DB_CREATE_PRIZES_TABLE);
			db.execSQL(DB_CREATE_CLUES_TABLE);
			Log.d(DEBUG_TAG, "Database creating...");
			Log.d(DEBUG_TAG, "Table " + DB_PRIZES_TABLE + " ver." + DB_VERSION + " created");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL(DROP_PRIZES_TABLE);
			db.execSQL(DROP_CLUES_TABLE);
			Log.d(DEBUG_TAG, "Database updating...");
			Log.d(DEBUG_TAG, "Table " + DB_PRIZES_TABLE + " updated from ver." + oldVersion
					+ " to ver." + newVersion);
			Log.d(DEBUG_TAG, "All data is lost.");

			onCreate(db);
		}
	}

	public static boolean doesDatabaseExist(Context context) {
		File dbFile = context.getDatabasePath(DB_NAME);
		return dbFile.exists();
	}

	public DbAdapter(Context context) {
		this.context = context;
	}

	public DbAdapter open() {

		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
			try {
				db = dbHelper.getWritableDatabase();
			} catch (SQLException e) {
				db = dbHelper.getReadableDatabase();
			}
		}
		return this;
	}

	public void close() {

		dbHelper.close();

	}

	public void clear() {
		db.execSQL(DROP_PRIZES_TABLE);
		db.execSQL(DROP_CLUES_TABLE);
		dbHelper.onCreate(db);
	}

	/* PRIZES METHODS */

	public long insertPrize(Prize prize) {
		ContentValues newPrizeValues = new ContentValues();
		newPrizeValues.put(KEY_ID, prize.id);
		newPrizeValues.put(KEY_NAME, prize.name);
		newPrizeValues.put(KEY_DESCRIPTION, prize.description);
		newPrizeValues.put(KEY_IMAGE, prize.image);
		newPrizeValues.put(KEY_POINTS, prize.points);
		newPrizeValues.put(KEY_AVAILABLE, prize.available ? 1 : 0);
		newPrizeValues.put(KEY_ADRESS, prize.address);
		long tmp = db.replace(DB_PRIZES_TABLE, null, newPrizeValues);
		return tmp;
	}

	public long insertPrizeAndRemoveNotPresent(Prize[] prizes) {

		LinkedList<Integer> keys = getAllKeysOfPrizes();
		boolean exists;
		for (Integer k : keys) {
			exists = false;
			for (Prize p : prizes) {
				if (p.id == k) {
					exists = true;
				}
			}
			if (!exists) {
				deletePrize(k);
			}
		}
		for (Prize prize : prizes) {
			insertPrize(prize);
		}
		return 0;
	}

	public boolean updatePrize(Prize prize) {
		return updatePrize(prize.id, prize.name, prize.description, prize.image, prize.points,
				prize.available, prize.address);
	}

	private boolean updatePrize(int localId, String name, String description, byte[] image,
			int points, boolean available, String adress) {
		String where = KEY_ID + "=" + localId;
		ContentValues updateTodoValues = new ContentValues();
		updateTodoValues.put(KEY_NAME, name);
		updateTodoValues.put(KEY_DESCRIPTION, description);
		updateTodoValues.put(KEY_IMAGE, image);
		updateTodoValues.put(KEY_POINTS, points);
		updateTodoValues.put(KEY_AVAILABLE, available ? 1 : 0);
		updateTodoValues.put(KEY_ADRESS, adress);
		boolean tmp = db.update(DB_PRIZES_TABLE, updateTodoValues, where, null) > 0;

		return tmp;
	}

	public boolean deletePrize(long id) {
		String where = KEY_ID + "=" + id;
		boolean tmp = db.delete(DB_PRIZES_TABLE, where, null) > 0;
		return tmp;
	}

	public Cursor getAllPrizes() {
		String[] columns = { KEY_ID, KEY_NAME, KEY_DESCRIPTION, KEY_IMAGE, KEY_POINTS,
				KEY_AVAILABLE, KEY_ADRESS };
		return db.query(DB_PRIZES_TABLE, columns, null, null, null, null, null);
	}

	public Cursor getAllPrizesKeys() {
		String[] columns = { KEY_ID };
		return db.query(DB_PRIZES_TABLE, columns, null, null, null, null, null);
	}

	public Prize getPrize(long sid) {
		String[] columns = { KEY_ID, KEY_NAME, KEY_DESCRIPTION, KEY_IMAGE, KEY_POINTS,
				KEY_AVAILABLE, KEY_ADRESS };
		String where = KEY_ID + "=" + sid;
		Cursor cursor = db.query(DB_PRIZES_TABLE, columns, where, null, null, null, null);
		Prize prize = null;
		if (cursor != null && cursor.moveToFirst()) {
			int id = cursor.getInt(ID_COLUMN);
			String name = cursor.getString(NAME_COLUMN);
			String content = cursor.getString(DESCRIPTION_COLUMN);
			byte[] image = cursor.getBlob(IMAGE_COLUMN);
			int points = cursor.getInt(POINTS_COLUMN);
			boolean available = cursor.getInt(AVAILABLE_COLUMN) == 1;
			String adress = cursor.getString(ADRESS_COLUMN);
			prize = new Prize(id, name, content, image, points, available, adress);
		}
		return prize;
	}

	public LinkedList<Prize> getAllPrizesList() {
		Cursor cursor = getAllPrizes();
		LinkedList<Prize> prizes = new LinkedList<Prize>();

		// LinkedList<Integer> ids = getAllKeysOfPrizes();
		//
		// for (Integer i : ids) {
		// Prize tmp = getPrize(i);
		// if (tmp != null) {
		// prizes.add(tmp);
		// }
		// }

		if (cursor != null && cursor.moveToFirst()) {
			do {
				int id = cursor.getInt(ID_COLUMN);
				String name = cursor.getString(NAME_COLUMN);
				String description = cursor.getString(DESCRIPTION_COLUMN);
				byte[] image = cursor.getBlob(IMAGE_COLUMN);
				int points = cursor.getInt(POINTS_COLUMN);
				boolean available = cursor.getInt(AVAILABLE_COLUMN) == 1;
				String adress = cursor.getString(ADRESS_COLUMN);
				prizes.add(new Prize(id, name, description, image, points, available, adress));

			} while (cursor.moveToNext());
		}
		return prizes;
	}

	public LinkedList<Integer> getAllKeysOfPrizes() {
		Cursor cursor = getAllPrizesKeys();
		LinkedList<Integer> keys = new LinkedList<Integer>();
		if (cursor != null && cursor.moveToFirst()) {
			do {
				int localId = cursor.getInt(ID_COLUMN);
				keys.add(localId);
			} while (cursor.moveToNext());
		}
		return keys;
	}

	/* Clues methods */

	public long insertClue(Clue clue) {
		ContentValues newClueValues = new ContentValues();
		newClueValues.put(KEY_ID, clue.id);
		newClueValues.put(KEY_CLUE_DESCRIPTION, clue.description);
		newClueValues.put(KEY_CLUE_POINTS, clue.points);
		newClueValues.put(KEY_CLUE_USED_POINTS, clue.usedPoints);
		newClueValues.put(KEY_LATITUDE, clue.latitude);
		newClueValues.put(KEY_LONGITUDE, clue.longitude);
		newClueValues.put(KEY_CLUE_AVAILABLE, clue.available ? 1 : 0);
		newClueValues.put(KEY_HASH, clue.hash);
		return db.replace(DB_CLUES_TABLE, null, newClueValues);
	}

	public long insertCluesAndRemoveNotPresent(Clue[] clues) {

		LinkedList<Integer> keys = getAllKeysOfClues();
		boolean exists;
		for (Integer k : keys) {
			exists = false;
			for (Clue p : clues) {
				if (p.id == k) {
					exists = true;
				}
			}
			if (!exists) {
				deleteClue(k);
			}
		}
		for (Clue clue : clues) {
			insertClue(clue);
		}
		return 0;
	}

	public boolean updateClue(Clue clue) {
		boolean tmp = updateClue(clue.id, clue.description, clue.points, clue.usedPoints,
				clue.latitude, clue.longitude, clue.available, clue.hash);

		return tmp;
	}

	private boolean updateClue(long localId, String description, int points, int usedPoints,
			double latitude, double longitude, boolean available, String hash) {
		String where = KEY_ID + "=" + localId;
		ContentValues newClueValues = new ContentValues();
		newClueValues.put(KEY_CLUE_DESCRIPTION, description);
		newClueValues.put(KEY_CLUE_POINTS, points);
		newClueValues.put(KEY_CLUE_USED_POINTS, usedPoints);
		newClueValues.put(KEY_LATITUDE, latitude);
		newClueValues.put(KEY_LONGITUDE, longitude);
		newClueValues.put(KEY_CLUE_AVAILABLE, available ? 1 : 0);
		newClueValues.put(KEY_HASH, hash);
		boolean tmp = db.update(DB_CLUES_TABLE, newClueValues, where, null) > 0;
		// notifyDbChanged(DbTables.Clues);
		return tmp;
	}

	public boolean deleteClue(long id) {
		String where = KEY_ID + "=" + id;
		boolean tmp = db.delete(DB_CLUES_TABLE, where, null) > 0;
		return tmp;
	}

	public Cursor getAllClues() {
		String[] columns = { KEY_ID, KEY_CLUE_DESCRIPTION, KEY_CLUE_POINTS, KEY_CLUE_USED_POINTS,
				KEY_LATITUDE, KEY_LONGITUDE, KEY_CLUE_AVAILABLE, KEY_HASH };
		return db.query(DB_CLUES_TABLE, columns, null, null, null, null, null);
	}

	public Cursor getAllCluesKeys() {
		String[] columns = { KEY_ID };
		return db.query(DB_CLUES_TABLE, columns, null, null, null, null, null);
	}

	public Clue getClue(long sid) {
		String[] columns = { KEY_ID, KEY_CLUE_DESCRIPTION, KEY_CLUE_POINTS, KEY_CLUE_USED_POINTS,
				KEY_LATITUDE, KEY_LONGITUDE, KEY_CLUE_AVAILABLE, KEY_HASH };
		String where = KEY_ID + "=" + sid;
		Cursor cursor = db.query(DB_CLUES_TABLE, columns, where, null, null, null, null);
		Clue clue = null;
		if (cursor != null && cursor.moveToFirst()) {
			long id = cursor.getLong(ID_COLUMN);
			String description = cursor.getString(CLUE_DESCRIPTION_COLUMN);
			int points = cursor.getInt(CLUE_POINTS_COLUMN);
			int usedPoints = cursor.getInt(CLUE_USED_POINTS_COLUMN);
			boolean available = cursor.getInt(CLUE_AVAILABLE_COLUMN) == 1;
			double latitude = cursor.getDouble(LATITUDE_COLUMN);
			double longitude = cursor.getDouble(LONGITUDE_COLUMN);
			String hash = cursor.getString(HASH_COLUMN);
			clue = new Clue(id, description, latitude, longitude, points, usedPoints, available,
					hash);
		}
		return clue;
	}

	public LinkedList<Clue> getAllCluesList() {
		Cursor cursor = getAllClues();
		LinkedList<Clue> clues = new LinkedList<Clue>();
		if (cursor != null && cursor.moveToFirst()) {
			do {
				long id = cursor.getLong(ID_COLUMN);
				String description = cursor.getString(CLUE_DESCRIPTION_COLUMN);
				int points = cursor.getInt(CLUE_POINTS_COLUMN);
				int usedPoints = cursor.getInt(CLUE_USED_POINTS_COLUMN);
				boolean available = cursor.getInt(CLUE_AVAILABLE_COLUMN) == 1;
				double latitude = cursor.getDouble(LATITUDE_COLUMN);
				double longitude = cursor.getDouble(LONGITUDE_COLUMN);
				String hash = cursor.getString(HASH_COLUMN);
				clues.add(new Clue(id, description, latitude, longitude, points, usedPoints,
						available, hash));
			} while (cursor.moveToNext());
		}
		return clues;
	}

	public LinkedList<Integer> getAllKeysOfClues() {
		Cursor cursor = getAllCluesKeys();
		LinkedList<Integer> keys = new LinkedList<Integer>();
		if (cursor != null && cursor.moveToFirst()) {
			do {
				int localId = cursor.getInt(ID_COLUMN);
				keys.add(localId);
			} while (cursor.moveToNext());
		}
		return keys;
	}

	public long getUserPoints() {
		LinkedList<Clue> clues = getAllCluesList();
		long points = 0;
		for (Clue c : clues) {
			if (!c.available) {
				points += c.points;
			}
		}
		return points;
	}

	public long getUserLeftPoints() {
		LinkedList<Clue> clues = getAllCluesList();
		long points = 0;
		for (Clue c : clues) {
			if (!c.available) {
				points += (c.points - c.usedPoints);
			}
		}
		return points;
	}
}
