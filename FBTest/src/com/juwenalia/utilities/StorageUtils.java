package com.juwenalia.utilities;

import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.holoeverywhere.preference.SharedPreferences.Editor;

import android.content.Context;

import com.juwenalia.utilities.Constants.UserTypes;

public abstract class StorageUtils {
	private static final String SHARED_PREFS = "SHARED_PREFS";
	private static final String USER_ID = "USER_ID";
	private static final String USER_ACTIVATED = "USER_ACTIVATED";
	private static final String APP_STATE_TIMESTAMP = "APP_STATE_TIMESTAMP";
	private static final String BASE_IMAGE_URL = "BASE_IMAGE_URL";
	private static final String USER_NAME = "USER_NAME";
	private static final String USER_TYPE = "USER_TYPE";
	private static final String USED_POINTS = "USED_POINTS";
	private static final String STOP_TIMESTAMP = "STOP_TIMESTAMP";

	private static SharedPreferences getSharedPreferences(final Context context) {
		return PreferenceManager.wrap(context.getApplicationContext(), SHARED_PREFS,
				Context.MODE_PRIVATE);
	}

	private static String getString(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getString(key, null);
	}

	private static boolean putString(final Context context, final String key, final String value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putString(key, value);
		return sharedPrefsEditor.commit();
	}

	private static boolean putInt(final Context context, final String key, final int value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putInt(key, value);
		return sharedPrefsEditor.commit();
	}

	private static int getInt(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getInt(key, -1);
	}

	private static boolean putLong(final Context context, final String key, final long value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putLong(key, value);
		return sharedPrefsEditor.commit();
	}

	private static long getLong(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getLong(key, -1);
	}

	private static boolean putBoolean(final Context context, final String key, final boolean value) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.putBoolean(key, value);
		return sharedPrefsEditor.commit();
	}

	private static boolean getBoolean(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.getBoolean(key, false);
	}

	public static boolean containsKey(final Context context, final String key) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		return sharedPrefs.contains(key);
	}

	public static void setUserId(Context context, String userId) {
		putString(context, USER_ID, userId);
	}

	public static String getUserId(Context context) {
		return getString(context, USER_ID);
	}

	public static void setUserName(Context context, String username) {
		putString(context, USER_NAME, username);
	}

	public static String getUserName(Context context) {
		return getString(context, USER_NAME);
	}

	public static void setUserActivated(Context context, boolean isActivated) {
		putBoolean(context, USER_ACTIVATED, isActivated);
	}

	public static boolean getUserActivated(Context context) {
		return getBoolean(context, USER_ACTIVATED);
	}

	public static void setUserType(Context context, UserTypes userType) {
		putInt(context, USER_TYPE, userType.getValue());
	}

	public static UserTypes getUserType(Context context) {
		int value = getInt(context, USER_TYPE);
		return UserTypes.intToType(value);
	}

	public static void setAppStateTimestamp(Context context, String timestamp) {
		putString(context, APP_STATE_TIMESTAMP, timestamp);
	}

	public static String getAppStateTimestamp(Context context) {
		return getString(context, APP_STATE_TIMESTAMP);
	}

	public static void setBaseImageUrl(Context context, String url) {
		putString(context, BASE_IMAGE_URL, url);
	}

	public static String getBaseImageUrl(Context context) {
		return getString(context, BASE_IMAGE_URL);
	}

	public static void clearStorage(Context context) {
		final SharedPreferences sharedPrefs = getSharedPreferences(context);
		final Editor sharedPrefsEditor = sharedPrefs.edit();
		sharedPrefsEditor.clear();
		sharedPrefsEditor.commit();

	}

	public static void setUsedPoints(Context context, int usedPoints) {
		putInt(context, USED_POINTS, usedPoints);
	}

	public static int getUsedPoints(Context context) {
		return getInt(context, USED_POINTS);

	}

	public static void setStopTimestamp(Context context, long value) {
		putLong(context, STOP_TIMESTAMP, value);
	}

	public static long getStopTimestamp(Context context) {
		return getLong(context, STOP_TIMESTAMP);
	}
}
