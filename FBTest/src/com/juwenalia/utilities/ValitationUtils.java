package com.juwenalia.utilities;

import java.util.regex.Pattern;

import android.content.Context;
import android.text.TextUtils;

import com.juwenalia.R;

public abstract class ValitationUtils {
	public static final int MIN_PASSWORD_LENGTH = 2;
	public static final int MAX_PASSWORD_LENGTH = 10;
	// private static final String EMAIL_PATTERN =
	// "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	// private static final String EMAIL_PATTERN =
	// "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\\b";
	private static final String EMAIL_PATTERN = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

	public static boolean validateUserName(String username) {
		// TODO: create user name validation
		return !TextUtils.isEmpty(username);
	}

	public static boolean validatePasswords(String password) {
		if (!TextUtils.isEmpty(password) && password.length() >= MIN_PASSWORD_LENGTH
				&& password.length() <= MAX_PASSWORD_LENGTH) {
			return true;
		}
		return false;
	}

	public static boolean validateEmail(String email) {

		// return true;

		if (TextUtils.isEmpty(email)) {
			return false;
		}

		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		return pattern.matcher(email).matches();
	}

	public static String codeToMessage(Context context, int code) {
		switch (code) {
		case 1:
			return context.getString(R.string.email_already_taken);
		case 2:
			return context.getString(R.string.wrong_credentials);
		default:
			return context.getString(R.string.unknown_error_code);
		}
	}
}
