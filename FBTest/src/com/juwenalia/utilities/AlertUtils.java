package com.juwenalia.utilities;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.juwenalia.R;

public abstract class AlertUtils {

	public static void showToast(Context context, String s, int duration) {
		Toast.makeText(context, s, duration).show();
	}

	public static void showToast(Context context, String s) {
		showToast(context, s, Toast.LENGTH_SHORT);
	}

	public static ProgressDialog produceProgressDialog(Context context, String message,
			OnCancelListener listener) {

		return produceProgressDialog(context, message, listener, true);
	}

	public static ProgressDialog produceProgressDialog(Context context, String message,
			OnCancelListener listener, boolean isCancelable) {

		final ProgressDialog dialog = new ProgressDialog(context);
		dialog.setMessage(message);
		dialog.setTitle(context.getString(R.string.app_title));
		dialog.setIndeterminate(true);
		dialog.setCancelable(isCancelable);
		dialog.setOnCancelListener(listener);
		return dialog;
	}

	public static AlertDialog showAlertDialog(Context context, String message) {

		AlertDialog tmp = produceAlertDialog(context, message, null,
				context.getString(R.string.ok), null, null, null, null, null, true);
		tmp.show();
		return tmp;
	}

	public static AlertDialog showAlertDialog(Context context, String message, String neutalText,
			OnClickListener neutralListener) {

		AlertDialog tmp = produceAlertDialog(context, message, null, null, null, neutalText, null,
				null, neutralListener, false);
		tmp.show();
		return tmp;
	}

	public static AlertDialog produceAlertDialog(Context context, String message, String title,
			String positivText, String negativeText, String neutralText,
			OnClickListener positiveOnClickListener, OnClickListener negativeOnClickListener,
			OnClickListener neutralOnClickListener, Boolean cancelable) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		if (TextUtils.isEmpty(title)) {
			alertDialogBuilder.setTitle(context.getString(R.string.app_title));
		} else {
			alertDialogBuilder.setTitle(title);
		}

		if (positivText != null) {
			alertDialogBuilder.setPositiveButton(positivText, positiveOnClickListener);
		}
		if (neutralText != null) {
			alertDialogBuilder.setNeutralButton(neutralText, neutralOnClickListener);
		}
		if (negativeText != null) {
			alertDialogBuilder.setNegativeButton(negativeText, negativeOnClickListener);
		}

		alertDialogBuilder.setCancelable(cancelable);

		TextView resultMessage = new TextView(context);
		resultMessage.setTextSize(20);
		resultMessage.setPadding(20, 25, 20, 25);
		resultMessage.setText(message);
		resultMessage.setGravity(Gravity.CENTER);
		alertDialogBuilder.setView(resultMessage);

		AlertDialog dl = alertDialogBuilder.create();
		return dl;
	}

	public static View viewFromLayout(Context context, int layout) {
		return LayoutInflater.from(context).inflate(layout);
	}

	public static AlertDialog produceAlertDialogWithCustomView(Context context, String title,
			String positivText, String negativeText, String neutralText,
			OnClickListener positiveOnClickListener, OnClickListener negativeOnClickListener,
			OnClickListener neutralOnClickListener, Boolean cancelable, View view) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		if (TextUtils.isEmpty(title)) {
			alertDialogBuilder.setTitle(context.getString(R.string.app_title));
		} else {
			alertDialogBuilder.setTitle(title);
		}

		if (positivText != null) {
			alertDialogBuilder.setPositiveButton(positivText, positiveOnClickListener);
		}
		if (neutralText != null) {
			alertDialogBuilder.setNeutralButton(neutralText, neutralOnClickListener);
		}
		if (negativeText != null) {
			alertDialogBuilder.setNegativeButton(negativeText, negativeOnClickListener);
		}

		alertDialogBuilder.setCancelable(cancelable);

		alertDialogBuilder.setView(view);

		AlertDialog dl = alertDialogBuilder.create();
		return dl;
	}
}
