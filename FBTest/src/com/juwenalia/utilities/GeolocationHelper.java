package com.juwenalia.utilities;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.juwenalia.R;
import com.juwenalia.listeners.LocationChangedListener;

public class GeolocationHelper {

	private LocationManager locationManager;
	private Location lastKnownGpsLocation = null;
	private Location lastKnownOtherLocation = null;
	private LocationChangedListener listener;
	private boolean locationQuality;

	public void setListener(LocationChangedListener listener) {
		this.listener = listener;
	}

	private LocationListener locationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onLocationChanged(Location location) {
			if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
				lastKnownGpsLocation = location;
				locationQuality = true;
			} else {
				lastKnownOtherLocation = location;
				locationQuality = false;
			}
			if (listener != null) {
				listener.onLocationChanged(getBestLocation(), locationQuality);
			}
		}
	};

	public GeolocationHelper(Context context) {
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			AlertUtils.showAlertDialog(context, context.getString(R.string.please_turn_on_gps));
		}
	}

	public void addListeners() {
		locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,
				Constants.LOCATION_CHECK_INTERVAL_MS, Constants.MIN_DISTANCE, locationListener);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
				Constants.LOCATION_CHECK_INTERVAL_MS, Constants.MIN_DISTANCE, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				Constants.LOCATION_CHECK_INTERVAL_MS, Constants.MIN_DISTANCE, locationListener);
	}

	public void removeListeners() {
		locationManager.removeUpdates(locationListener);
	}

	public Location getBestLocation() {
		if (lastKnownGpsLocation != null) {
			return lastKnownGpsLocation;
		} else if (lastKnownOtherLocation != null) {
			return lastKnownOtherLocation;
		} else {
			return null;
		}

	}
}
