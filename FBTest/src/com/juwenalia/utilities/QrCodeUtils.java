package com.juwenalia.utilities;

import android.content.Context;
import android.location.Location;

import com.juwenalia.R;
import com.juwenalia.tasks.AddCodeAsyncTask;
import com.juwenalia.ws.models.Clue;
import com.juwenalia.ws.models.Prize;

public abstract class QrCodeUtils {
	public enum CodeTypes {
		NormalCode, PrizeCode, InfoCode, NotACode
	}

	public static CodeTypes detectCodeType(String code) {
		if (code.startsWith("0")) {
			return CodeTypes.NormalCode;
		} else if (code.startsWith("1")) {
			return CodeTypes.PrizeCode;
		} else if (code.startsWith("2")) {
			return CodeTypes.InfoCode;
		}
		return CodeTypes.NotACode;
	}

	public static void processNormalCode(Context context, String code, Location location) {
		if (SecurityUtils.isLocationFake(context)) {
			AlertUtils.showAlertDialog(context,
					context.getString(R.string.prevent_fake_location_info));
		} else {
			DbAdapter adapter = new DbAdapter(context);
			adapter.open();
			try {
				String[] tmp = code.split(" ");
				if (tmp.length == 3) {
					long id = Long.parseLong(tmp[1]);

					Clue clue = adapter.getClue(id);

					if (clue != null && clue.hash.equalsIgnoreCase(tmp[2])) {
						if (clue.available) {
							Location targetLocation = clue.getLocation();
							if (location != null) {
								float distance = location.distanceTo(targetLocation);
								if (distance <= Constants.APPROVED_DISTANCE) {
									clue.available = false;
									new AddCodeAsyncTask(context, null, id).execute();
									adapter.updateClue(clue);
									AlertUtils.showAlertDialog(context,
											context.getString(R.string.user_found_code));
								} else {
									AlertUtils.showAlertDialog(context, context
											.getString(R.string.user_to_far_from_code_location));
								}
							} else {
								AlertUtils.showAlertDialog(context,
										context.getString(R.string.cant_find_location));
							}
						} else {
							AlertUtils.showAlertDialog(context,
									context.getString(R.string.code_already_found));
						}
					} else if (clue.available) {
						AlertUtils.showAlertDialog(context,
								context.getString(R.string.code_not_in_db));
					}
				}
			} catch (Exception e) {
				AlertUtils.showAlertDialog(context, context.getString(R.string.cant_process_code));
			}
			adapter.close();
		}
	}

	public static Prize processPrizeCode(Context context, String code) {

		DbAdapter adapter = new DbAdapter(context);
		adapter.open();
		Prize prize = null;
		try {
			String[] tmp = code.split("_");
			if (tmp.length == 2) {
				long id = Long.parseLong(tmp[1].trim());
				prize = adapter.getPrize(id);
			}
		} catch (Exception e) {
			AlertUtils.showAlertDialog(context, context.getString(R.string.cant_process_code));
		}
		adapter.close();
		if (prize != null && prize.available) {
			return prize;
		} else {
			return null;
		}
	}

	public static void processInfoCode(Context context, String code) {
		try {
			String[] tmp = code.split("_");
			if (tmp.length == 2) {
				AlertUtils.showAlertDialog(context, tmp[1]);

			}
		} catch (Exception e) {
			AlertUtils.showAlertDialog(context, context.getString(R.string.cant_process_code));
		}
	}
}
