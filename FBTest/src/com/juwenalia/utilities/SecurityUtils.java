package com.juwenalia.utilities;

import java.security.MessageDigest;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

public abstract class SecurityUtils {

	private static final String S = "O3zv2kLYzl5jf16Fa";
	private static final byte[] SALT = { 1, 2, 3, 4, 5, 6, 7, 8 };

	public static String hashText(String input) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.reset();

			byte[] byteData = digest.digest(input.getBytes("UTF-8"));
			StringBuffer sb = new StringBuffer();

			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (Exception e) {
			Log.println(Log.ERROR, "JuwTest", e.getMessage());
			return null;
		}
	}

	public static boolean isLocationFake(Context context) {
		if (Settings.Secure.getString(context.getContentResolver(),
				Settings.Secure.ALLOW_MOCK_LOCATION).equals("0"))
			return false;
		else
			return true;
	}

	// private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception
	// {
	// SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
	// Cipher cipher = Cipher.getInstance("AES");
	// cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
	// byte[] encrypted = cipher.doFinal(clear);
	// return encrypted;
	// }
	//
	// private static byte[] decrypt(byte[] raw, byte[] encrypted) throws
	// Exception {
	// SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
	// Cipher cipher = Cipher.getInstance("AES");
	// cipher.init(Cipher.DECRYPT_MODE, skeySpec);
	// byte[] decrypted = cipher.doFinal(encrypted);
	// return decrypted;
	// }
	//
	// public static String encryptString(String content) {
	// //byte[] byteContent = content.getBytes();
	// //byte[] key = S.getBytes();
	// //byte[] result;
	// try {
	// String str = encrypt(content, S, SALT);
	// // result = encrypt(key, byteContent);
	// // String str = byteArrayToHex(result);
	// return str;
	// } catch (Exception e) {
	// return null;
	// }
	// }

	// public static String encode(String message){
	// byte [] b= message.getBytes();
	// String hexString = byteArrayToHex(b);
	// String res = cesar(hexString, 1);
	// return res;
	// }
	//
	// public static String decode(String message){
	// String mes = cesar(message, -1);
	// byte[] b;
	// try {
	// b = hexToByteArray(mes);
	// String res = new String(b);
	// return res;
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// return null;
	// }
	// }
	//
	// private static String cesar(String message, int offset){
	// StringBuilder sb = new StringBuilder();
	// int i=0;
	// for (char c : message.toCharArray()) {
	// i%=5;
	// sb.append((char) (c + offset*i));
	// i++;
	// }
	// return sb.toString();
	// }

	// public static String encrypt(String msg, String pwd, byte[] salt)
	// throws Exception {
	// // Map<String,String> retval = new HashMap<String,String>();
	//
	// // prepare to use PBKDF2/HMAC+SHA1, since ruby supports this readily
	// SecretKeyFactory factory =
	// SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	// // our key is 256 bits, and can be generated knowing the password and
	// salt
	// KeySpec spec = new PBEKeySpec(pwd.toCharArray(), salt, 1024, 256);
	// SecretKey tmp = factory.generateSecret(spec);
	// SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");
	//
	// // given key above, our cippher will be aes-256-cbc in ruby/openssl
	// Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	// cipher.init(Cipher.ENCRYPT_MODE, secret);
	// AlgorithmParameters params = cipher.getParameters();
	//
	// // generate the intialization vector
	// byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
	// // retval.put("iv", byteArrayToHex(iv));
	//
	// byte[] ciphertext = cipher.doFinal(msg.getBytes("UTF-8"));
	// // retval.put("encrypted", byteArrayToHex(ciphertext));
	// String res = byteArrayToHex(iv)+"_"+byteArrayToHex(ciphertext);
	// return res;
	// }
	//
	//
	// public static String decrypt(String msg, String pwd, byte[] salt)
	// throws Exception {
	// // Map<String,String> retval = new HashMap<String,String>();
	//
	// String [] split = msg.split("_");
	//
	// byte [] iv = hexToByteArray(split[0]);
	// byte [] content = hexToByteArray(split[1]);
	//
	// // prepare to use PBKDF2/HMAC+SHA1, since ruby supports this readily
	// SecretKeyFactory factory =
	// SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	// // our key is 256 bits, and can be generated knowing the password and
	// salt
	// KeySpec spec = new PBEKeySpec(pwd.toCharArray(), salt, 1024, 256);
	// SecretKey tmp = factory.generateSecret(spec);
	// SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");
	//
	// // given key above, our cippher will be aes-256-cbc in ruby/openssl
	// Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	// IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
	// cipher.init(Cipher.DECRYPT_MODE, secret,ivParameterSpec);
	// AlgorithmParameters params = cipher.getParameters();
	//
	// // generate the intialization vector
	// // byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
	// // retval.put("iv", byteArrayToHex(iv));
	//
	// byte[] ciphertext = cipher.doFinal(content);//msg.getBytes("UTF-8"));
	// // retval.put("encrypted", byteArrayToHex(ciphertext));
	// String res = new String(ciphertext,
	// "UTF-8");//byteArrayToHex(iv)+"_"+byteArrayToHex(ciphertext);
	// return res;
	// }
	//
	// public static String dencryptString(String content) {
	// byte[] key = S.getBytes();
	// byte[] result;
	// try {
	// // byte[] byteContent = hexToByteArray(content);
	// // result = decrypt(key, byteContent);
	// // String str = new String(result, "UTF-8");
	// // return str;
	// String str= decrypt(content, S, SALT);
	// return str;
	// } catch (Exception e) {
	// return null;
	// }
	// }

	// private static String byteArrayToHex(byte[] a) {
	// StringBuilder sb = new StringBuilder();
	// for (byte b : a) {
	// sb.append(String.format("%02x", b & 0xff));
	// }
	// return sb.toString();
	// }
	//
	// private static byte[] hexToByteArray(String s) throws Exception {
	// if (s.length() % 2 != 0) {
	// throw new Exception("Wrong hex size");
	// }
	// int len = s.length();
	// byte[] data = new byte[len / 2];
	// for (int i = 0; i < len; i += 2) {
	// data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) +
	// Character.digit(
	// s.charAt(i + 1), 16));
	// }
	// return data;
	//
	// }
	// if(hex.length()%2!=0){
	// throw new Exception("Wrong hex size");
	// }
	// byte [] result= new byte [hex.length()/2];
	// String tmp;
	// for(int i=0;i<hex.length();i+=2){
	// tmp = hex.substring(i, i+2);
	// result[i/2]= Byte.parseByte(tmp);
	// }
	// return result;
	// }
}
