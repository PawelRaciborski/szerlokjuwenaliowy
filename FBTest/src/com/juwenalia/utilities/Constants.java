package com.juwenalia.utilities;

public abstract class Constants {

	public static final float APPROVED_DISTANCE = 200.0f;
	public static final long LOCATION_CHECK_INTERVAL_MS = 60000;
	public static final long MIN_DISTANCE = 50;
	public static final String GAME_RULES = "https://dl.dropboxusercontent.com/u/8192112/regulamin1.pdf";
	public static final String SZERLOK_RULES = "https://dl.dropboxusercontent.com/u/8192112/regulamin0.pdf";

	public static final long REFRESH_INTERVAL_MS = 3600000;
	// public static final long REFRESH_INTERVAL_MS = 30000;
	public static final long REFRESH_MARGIN_MS = 60000;
	public static final long REFRESH_ADDITIONAL_MS = 300000;

	// public static final long REFRESH_ADDITIONAL_MS = 30000;

	public enum UserTypes {

		NORMAL(0), FACEBOOK(1), GAME(2);

		private int _value;

		UserTypes(int Value) {
			this._value = Value;
		}

		public int getValue() {
			return _value;
		}

		public static UserTypes intToType(int i) {
			if (i == 0) {
				return NORMAL;
			} else if (i == 1) {
				return FACEBOOK;
			} else if (i == 2) {
				return GAME;
			} else {
				return null;
			}
		}
	}
}
