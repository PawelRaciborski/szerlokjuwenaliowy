package com.juwenalia.tasks;

import java.util.LinkedList;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.ws.WebServiceProvider;

public class AppStateAsyncTask extends BaseAsyncTask {

	private LinkedList<Long> clues;

	public AppStateAsyncTask(Context context, AsyncTaskListener listener, LinkedList<Long> clues) {
		super(context, listener, false, false);
		this.clues = clues;
	}

	@Override
	protected Object doInBackground(Object... params) {
		Object result = null;
		try {
			String userId = StorageUtils.getUserId(context);
			String timestamp = StorageUtils.getAppStateTimestamp(context);
			// TODO: getting ids form DB
			// LinkedList<Long> clues = null;
			result = WebServiceProvider.getInstance().callAppState(userId, clues, timestamp);
		} catch (Exception e) {
			result = e;
		}
		return handleRequestResult(result);
	}
}
