package com.juwenalia.tasks;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.ws.WebServiceProvider;

public class AddCodeAsyncTask extends BaseAsyncTask {
	private Long codeId;

	public AddCodeAsyncTask(Context context, AsyncTaskListener listener, long codeId) {
		super(context, listener, false, false);
		this.codeId = codeId;
	}

	@Override
	protected Object doInBackground(Object... params) {
		Object result = null;
		try {
			String userId = StorageUtils.getUserId(context);
			result = WebServiceProvider.getInstance().callAddCode(userId, codeId);
		} catch (Exception e) {
			result = e;
		}
		return handleRequestResult(result);
	}
}
