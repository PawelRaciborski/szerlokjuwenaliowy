package com.juwenalia.tasks;

import java.io.IOException;
import java.util.HashMap;

import android.content.Context;
import android.text.TextUtils;

import com.juwenalia.R;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.ws.WebServiceProvider;
import com.juwenalia.ws.models.Clue;
import com.juwenalia.ws.models.Prize;

public class ProcessAppStateAsyncTask extends BaseAsyncTask {

	protected Prize[] prizes;
	protected Clue[] clues;

	public ProcessAppStateAsyncTask(Context context, AsyncTaskListener listener, Prize[] prizes,
			Clue[] clues) {
		super(context, listener, false, false);
		this.prizes = prizes;
		this.clues = clues;
	}

	@Override
	protected Object doInBackground(Object... arg0) {
		DbAdapter adapter = new DbAdapter(context);
		adapter.open();

		if (prizes != null && prizes.length > 0) {
			HashMap<String, byte[]> images = new HashMap<String, byte[]>();
			for (Prize p : prizes) {
				if (images.containsKey(p.image_url)) {
					p.image = images.get(p.image_url);
				} else {
					try {
						if (!TextUtils.isEmpty(p.image_url)) {
							byte[] tmp = WebServiceProvider.getInstance().downloadImage(context,
									p.image_url);
							images.put(p.image_url, tmp);
							p.image = tmp;
						}
					} catch (IOException e) {
						p.image = null;
					}
				}
			}
			adapter.insertPrizeAndRemoveNotPresent(prizes);
		}
		if (clues != null && clues.length > 0) {
			adapter.insertCluesAndRemoveNotPresent(clues);
		}
		adapter.close();
		return true;
	}

	@Override
	protected void onPostExecute(Object result) {
		if (showProgress) {
			progressDialog.dismiss();
		}
		if (result != null) {
			onSuccess(result);
		} else {
			onFailure(R.string.unknown_problem);
		}
	}
}
