package com.juwenalia.tasks;

import org.holoeverywhere.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.juwenalia.R;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.utilities.AlertUtils;
import com.juwenalia.ws.models.BaseResponseJson;

public class BaseAsyncTask extends AsyncTask<Object, Object, Object> implements OnCancelListener {

	protected Context context;
	protected AsyncTaskListener listener;
	protected ProgressDialog progressDialog;
	protected boolean isCancelable;
	protected boolean canceled = false;
	protected boolean showProgress;

	protected BaseAsyncTask(Context context, AsyncTaskListener listener, boolean isCancelable,
			boolean showProgress) {
		this.isCancelable = isCancelable;
		this.context = context;
		this.listener = listener;
		this.showProgress = showProgress;
	}

	@Override
	protected void onCancelled() {
		if (listener != null) {
			listener.onCancel();
		}
		super.onCancelled();
	}

	@Override
	protected void onPreExecute() {
		if (showProgress) {
			progressDialog = AlertUtils.produceProgressDialog(context,
					context.getString(R.string.please_wait), this, this.isCancelable);
			progressDialog.show();
		}
		super.onPreExecute();

	}

	protected void onSuccess(Object o) {
		if (!canceled && listener != null) {
			listener.onSuccess(o);
		}
	}

	protected void onFailure(Object o) {
		if (!canceled && listener != null) {
			listener.onFailure(o);
		}
	}

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	protected void onPostExecute(Object result) {
		if (showProgress) {
			progressDialog.dismiss();
		}
		super.onPostExecute(result);
		if (result != null) {
			if (result instanceof BaseResponseJson) {
				onSuccess(result);
			} else if (result instanceof Exception) {
				onFailure(((Exception) result).getMessage());
			} else {
				onFailure(result.toString());
			}
		} else {
			onFailure(R.string.unknown_problem);
		}
	}

	// Override to handle different codes.
	protected Object handleRequestResult(Object result) {

		if (result != null && result instanceof BaseResponseJson) {
			if (((BaseResponseJson) result).code != 0) {
				String message = ((BaseResponseJson) result).message;
				if (TextUtils.isEmpty(message)) {
					return message = "Response code:" + ((BaseResponseJson) result).code;
				}
				return new Exception(message);
			}
			if (((BaseResponseJson) result).isResponseValid()) {
				return result;
			} else {
				return new Exception(context.getString(R.string.wrong_server_response));
			}
		}
		return result;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		canceled = true;
		cancel(true);
	}

}
