package com.juwenalia.tasks;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.ws.WebServiceProvider;

public class UserActivatedAsyncTask extends BaseAsyncTask {

	private String userId;

	public UserActivatedAsyncTask(Context context, AsyncTaskListener listener, String userId) {
		super(context, listener, false, true);
		this.userId = userId;
	}

	@Override
	protected Object doInBackground(Object... params) {
		Object result = null;
		try {
			result = WebServiceProvider.getInstance().callCheckActivatedWebservice(userId);
		} catch (Exception e) {
			result = e;
		}
		return handleRequestResult(result);
	}

}
