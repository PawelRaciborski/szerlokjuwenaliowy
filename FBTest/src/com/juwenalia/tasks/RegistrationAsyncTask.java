package com.juwenalia.tasks;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.utilities.Constants.UserTypes;
import com.juwenalia.ws.WebServiceProvider;

public class RegistrationAsyncTask extends BaseAsyncTask {

	private String username, email, password, facebookId;
	private UserTypes type;

	public RegistrationAsyncTask(Context context, AsyncTaskListener listener, String username,
			String email, String password, String facebookId, UserTypes type) {
		super(context, listener, true, true);
		this.username = username;
		this.email = email;
		this.password = password;
		this.facebookId = facebookId;
		this.type = type;
	}

	@Override
	protected Object doInBackground(Object... params) {
		Object result = null;
		try {
			result = WebServiceProvider.getInstance().callRegistrationWebsercive(username, email,
					password, facebookId, type);
		} catch (Exception e) {
			result = e;
		}
		return handleRequestResult(result);
	}
}
