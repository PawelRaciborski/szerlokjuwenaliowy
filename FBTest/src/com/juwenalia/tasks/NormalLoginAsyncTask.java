package com.juwenalia.tasks;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.ws.WebServiceProvider;

public class NormalLoginAsyncTask extends BaseAsyncTask {

	private String email, password;

	public NormalLoginAsyncTask(Context context, AsyncTaskListener listener, String email,
			String password) {
		super(context, listener, true, true);
		this.email = email;
		this.password = password;
	}

	@Override
	protected Object doInBackground(Object... params) {
		Object result = null;
		try {
			result = WebServiceProvider.getInstance().callNormalLoginWebsercive(email, password);
		} catch (Exception e) {
			result = e;
		}
		return handleRequestResult(result);
	}
}
