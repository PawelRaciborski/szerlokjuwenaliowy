package com.juwenalia.tasks;

import java.util.LinkedList;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.ws.WebServiceProvider;
import com.juwenalia.ws.models.Clue;

public class GrantPrizeAsyncTask extends BaseAsyncTask {
	private Long codeId;

	public GrantPrizeAsyncTask(Context context, AsyncTaskListener listener, long codeId) {
		super(context, listener, false, true);
		this.codeId = codeId;
	}

	@Override
	protected Object doInBackground(Object... params) {
		Object result = null;
		try {
			String userId = StorageUtils.getUserId(context);
			DbAdapter adapter = new DbAdapter(context);
			adapter.open();
			LinkedList<Clue> cl = adapter.getAllCluesList();
			adapter.close();
			LinkedList<Long> usedId = null;
			if (cl != null && cl.size() > 0) {
				usedId = new LinkedList<Long>();
				for (Clue c : cl) {
					if (!c.available) {
						usedId.add(c.id);
					}
				}
			}
			result = WebServiceProvider.getInstance().callGrantPrize(userId, usedId, codeId);
		} catch (Exception e) {
			result = e;
		}
		return handleRequestResult(result);
	}
}
