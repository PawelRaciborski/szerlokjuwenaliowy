package com.juwenalia.tasks;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.ws.WebServiceProvider;

public class RankingAsyncTask extends BaseAsyncTask {

	public RankingAsyncTask(Context context, AsyncTaskListener listener) {
		super(context, listener, true, true);
	}

	@Override
	protected Object doInBackground(Object... params) {
		Object result = null;
		try {
			result = WebServiceProvider.getInstance().callGetRankingWebsercive(StorageUtils.getUserType(context));
		} catch (Exception e) {
			result = e;
		}
		return handleRequestResult(result);
	}

}
