package com.juwenalia.tasks;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import android.content.Context;

import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.ws.WebServiceProvider;

public class TestAsyncTask extends BaseAsyncTask {

	private int count;

	public TestAsyncTask(int count, Context context, AsyncTaskListener listener) {
		super(context, listener, true, true);
		this.count = count;
	}

	@Override
	protected Object doInBackground(Object... params) {
		super.doInBackground(params);
		for (int i = 0; i < count; i++) {
			try {
				Thread.sleep(5000);
				String test = WebServiceProvider.getInstance().testConnection();
				onSuccess(test);
				return test;
			} catch (InterruptedException e) {
				onFailure(e);
			} catch (ClientProtocolException e) {
				onFailure(e);
			} catch (IOException e) {
				onFailure(e);
			}
		}
		return null;
	}

}
