package com.juwenalia.fragments;

import java.util.LinkedList;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.adapters.PrizesAdapter;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.tasks.BaseAsyncTask;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.ws.models.Prize;

public class PrizesFragment extends Fragment {

	ListView listPrizes;

	Prize[] contentArray;
	PrizesAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.prizes, container, false);
		listPrizes = (ListView) view.findViewById(R.id.listPrizes);
		return view;
	}

	public void loadPrizes() {

		LoadPrizesAsyncTask task = new LoadPrizesAsyncTask(getSupportActivity(),
				new AsyncTaskListener() {

					@Override
					public void onSuccess(Object result) {
						adapter = new PrizesAdapter(getSupportActivity(), R.layout.prize_item,
								contentArray);
						listPrizes.setAdapter(adapter);
					}

					@Override
					public void onFailure(Object result) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub

					}
				});
		task.execute();

	}

	private class LoadPrizesAsyncTask extends BaseAsyncTask {

		protected LoadPrizesAsyncTask(Context context, AsyncTaskListener listener) {
			super(context, listener, false, false);
		}

		@Override
		protected Object doInBackground(Object... params) {
			DbAdapter dbAdapter = new DbAdapter(getSupportActivity());
			dbAdapter.open();
			LinkedList<Prize> prizes = dbAdapter.getAllPrizesList();
			dbAdapter.close();
			contentArray = new Prize[prizes.size()];

			for (int i = 0; i < prizes.size(); i++) {
				contentArray[i] = prizes.get(i);
			}

			return contentArray;
		}

		@Override
		protected void onPostExecute(Object result) {
			if (result != null) {
				onSuccess(result);
			} else {
				onFailure(result);
			}
		}
	}

}
