package com.juwenalia.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.ImageButton;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.listeners.SwitchFragmentListener;
import com.juwenalia.utilities.Constants.UserTypes;
import com.juwenalia.utilities.StorageUtils;

public class StartFragment extends Fragment implements OnClickListener {

	private SwitchFragmentListener switchFragmentListener;
	private ImageButton btnScan;
	private Button btnPrizesNav, btnCluesNav, btnTopListNav;

	public static final int SCAN = 0, PRIZES = 1, CLUES = 2, TOP = 3;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.start, container, false);
		// listClues = (ListView) view.findViewById(R.id.listClues);
		btnPrizesNav = (Button) view.findViewById(R.id.btnPrizesNav);
		btnPrizesNav.setOnClickListener(this);

		btnScan = (ImageButton) view.findViewById(R.id.btnScan);
		btnScan.setOnClickListener(this);
		btnCluesNav = (Button) view.findViewById(R.id.btnCluesNav);
		btnCluesNav.setOnClickListener(this);
		btnTopListNav = (Button) view.findViewById(R.id.btnTopListNav);
		btnTopListNav.setOnClickListener(this);

		if (StorageUtils.getUserType(getSupportActivity()) == UserTypes.GAME) {
			btnPrizesNav.setVisibility(View.GONE);
			btnTopListNav.setVisibility(View.GONE);
		}

		return view;
	}

	public SwitchFragmentListener getSwitchFragmentListener() {
		return switchFragmentListener;
	}

	public void setSwitchFragmentListener(SwitchFragmentListener switchFragmentListener) {
		this.switchFragmentListener = switchFragmentListener;
	}

	@Override
	public void onClick(View v) {
		if (v == btnScan) {
			buttonActions(SCAN);
		} else if (v == btnPrizesNav) {
			buttonActions(PRIZES);
		} else if (v == btnCluesNav) {
			buttonActions(CLUES);
		} else if (v == btnTopListNav) {
			buttonActions(TOP);
		}

	}

	private void buttonActions(int id) {
		if (switchFragmentListener != null) {
			switchFragmentListener.switchFragment(id);
		}
	}

}
