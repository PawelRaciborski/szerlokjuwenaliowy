package com.juwenalia.fragments;

import org.holoeverywhere.app.Fragment;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.juwenalia.R;

public class SplashFragment extends Fragment {

	@Override
	public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.splash, container, false);
		// LoginButton authButton = (LoginButton)
		// view.findViewById(R.id.login_button);
		// authButton.setFragment(this);
		// authButton.setReadPermissions(Arrays.asList("email", "user_status"));
		return view;
	}

	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	// View view = inflater.inflate(R.layout.splash, container,false);
	// return view;
	// }
}
