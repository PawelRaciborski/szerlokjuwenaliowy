package com.juwenalia.fragments;

import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.TextView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.juwenalia.R;

public class SelectionFragment extends Fragment implements OnClickListener {

	private ProfilePictureView profilePictureView;
	private TextView userNameView, txtKupon;
	private Button btnScan;
	private View view;

	@Override
	public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		view = inflater.inflate(R.layout.selection, container, false);
		// Find the user's profile picture custom view
		profilePictureView = (ProfilePictureView) view.findViewById(R.id.selection_profile_pic);
		profilePictureView.setCropped(true);

		// Find the user's name view
		userNameView = (TextView) view.findViewById(R.id.selection_user_name);
		txtKupon = (TextView) view.findViewById(R.id.txt_kupon);

		btnScan = (Button) view.findViewById(R.id.btn_scan);
		btnScan.setOnClickListener(this);

		// Check for an open session
		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			// Get the user's data
			makeMeRequest(session);
		}

		return view;
	}

	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	// super.onCreateView(inflater, container, savedInstanceState);
	// view = inflater.inflate(R.layout.selection,
	// container, false);
	// // Find the user's profile picture custom view
	// profilePictureView = (ProfilePictureView)
	// view.findViewById(R.id.selection_profile_pic);
	// profilePictureView.setCropped(true);
	//
	// // Find the user's name view
	// userNameView = (TextView) view.findViewById(R.id.selection_user_name);
	// txtKupon = (TextView) view.findViewById(R.id.txt_kupon);
	//
	// btnScan = (Button) view.findViewById(R.id.btn_scan);
	// btnScan.setOnClickListener(this);
	//
	// // Check for an open session
	// Session session = Session.getActiveSession();
	// if (session != null && session.isOpened()) {
	// // Get the user's data
	// makeMeRequest(session);
	// }
	//
	// return view;
	// }

	private void makeMeRequest(final Session session) {
		// Make an API call to get user data and define a
		// new callback to handle the response.
		Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
			@Override
			public void onCompleted(GraphUser user, Response response) {
				// If the response is successful
				if (session == Session.getActiveSession()) {
					if (user != null) {
						// Set the id for the ProfilePictureView
						// view that in turn displays the profile picture.
						profilePictureView.setProfileId(user.getId());
						// ;
						// Set the Textview's text to the user's name.
						userNameView.setText(user.getName() + " " + user.getId() + " "
								+ user.getProperty("email").toString() + " " + user.getUsername()
								+ " " + user.getLink());
					}
				}
				if (response.getError() != null) {
					// Handle errors, will do so later.
				}
			}
		});
		request.executeAsync();
	}

	private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
		if (session != null && session.isOpened()) {
			// Get the user's data.
			makeMeRequest(session);
		}
	}

	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state, final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	private static final int REAUTH_ACTIVITY_CODE = 100;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		txtKupon.setVisibility(View.GONE);
		if (requestCode == REAUTH_ACTIVITY_CODE) {
			uiHelper.onActivityResult(requestCode, resultCode, data);
		}

		if (requestCode == 0) {
			// IntentResult scanResult =
			// IntentIntegrator.parseActivityResult(requestCode, resultCode,
			// data);
			if (resultCode == Activity.RESULT_OK) {

				String result = data.getStringExtra("SCAN_RESULT");
				if (result.equalsIgnoreCase("pawel")) {
					txtKupon.setText("Talon na hotdoga");
				} else if (result.equalsIgnoreCase("gawel")) {
					txtKupon.setText("Talon na kebaba");
				} else {
					txtKupon.setText("Talon na fig� z makiem");
				}

				// tvStatus.setText(data.getStringExtra("SCAN_RESULT_FORMAT"));
				// tvResult.setText(data.getStringExtra("SCAN_RESULT"));
			} else if (resultCode == Activity.RESULT_CANCELED) {
				txtKupon.setText("Skanowanie anulowane");
				// tvStatus.setText("Press a button to start a scan.");
				// tvResult.setText("Scan cancelled.");
			}
			txtKupon.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		uiHelper.onSaveInstanceState(bundle);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onClick(View v) {

		try {

			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
			startActivityForResult(intent, 0);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// Toast.makeText(getApplicationContext(), "ERROR:" + e, 1).show();

		}
	}
}
