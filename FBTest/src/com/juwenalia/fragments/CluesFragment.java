package com.juwenalia.fragments;

import java.util.LinkedList;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.adapters.ClueAdapter;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.tasks.BaseAsyncTask;
import com.juwenalia.utilities.AlertUtils;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.ws.models.Clue;

public class CluesFragment extends Fragment {

	private ListView listClues;
	Clue[] contentArray;
	private ClueAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.clues, container, false);
		listClues = (ListView) view.findViewById(R.id.listClues);
		loadClues(null);
		return view;
	}

	public void loadClues(final Location userLocation) {
		LoadCluesAsyncTask task = new LoadCluesAsyncTask(getSupportActivity(),
				new AsyncTaskListener() {

					@Override
					public void onSuccess(Object result) {
						adapter = new ClueAdapter(getSupportActivity(), R.layout.clue_item,
								(Clue[]) result, userLocation);
						listClues.setAdapter(adapter);
					}

					@Override
					public void onFailure(Object result) {
						if (userLocation == null) {
							AlertUtils
									.showToast(
											getSupportActivity(),
											"Brak danych o lokalizacji. Poczekaj chwil�, a� urz�denie znajdzie lokalizacj�, a je�li to nie pomo�e, spr�buj na chwil� wyj�� przed budynek(je�li w nim jeste�).",
											Toast.LENGTH_LONG);
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub

					}
				}, userLocation);
		task.execute();

	}

	private class LoadCluesAsyncTask extends BaseAsyncTask {

		protected LoadCluesAsyncTask(Context context, AsyncTaskListener listener, Location location) {
			super(context, listener, false, false);
		}

		@Override
		protected Object doInBackground(Object... params) {
			DbAdapter dbAdapter = new DbAdapter(getSupportActivity());
			dbAdapter.open();
			LinkedList<Clue> clues = dbAdapter.getAllCluesList();
			dbAdapter.close();
			contentArray = new Clue[clues.size()];

			for (int i = 0; i < clues.size(); i++) {
				contentArray[i] = clues.get(i);
			}
			return contentArray;
		}

		@Override
		protected void onPostExecute(Object result) {
			if (result != null) {
				onSuccess(result);
			} else {
				onFailure(result);
			}
		}
	}
}
