package com.juwenalia.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ImageButton;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.utilities.AlertUtils;

public class AboutFragment extends Fragment implements OnClickListener {

	ImageButton imageButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.about, container, false);
		imageButton = (ImageButton) view.findViewById(R.id.btnAboutUs);
		imageButton.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		if (v == imageButton) {
			AlertUtils.produceAlertDialog(getSupportActivity(),
					getSupportActionBarContext()
							.getString(R.string.would_like_to_visit_our_website), null, getString(R.string.yes),
					getString(R.string.no), null, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							String url = "https://www.facebook.com/wildeastcoders";
							Intent i = new Intent(Intent.ACTION_VIEW);
							i.setData(Uri.parse(url));
							startActivity(i);
						}
					}, null, null, true).show();
		}
	}
}
