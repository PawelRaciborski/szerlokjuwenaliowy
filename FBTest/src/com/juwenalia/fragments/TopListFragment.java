package com.juwenalia.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.adapters.TopListAdapter;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.tasks.RankingAsyncTask;
import com.juwenalia.utilities.AlertUtils;
import com.juwenalia.ws.WebServiceProvider;
import com.juwenalia.ws.models.RankingResponse;

public class TopListFragment extends Fragment {
	private ListView listTopUsers;
	private TopListAdapter adapter;
	private AsyncTaskListener asyncTaskListener = new AsyncTaskListener() {

		@Override
		public void onSuccess(Object result) {
			if (result != null) {

				adapter = new TopListAdapter(getSupportActivity(), R.layout.clue_item,
						((RankingResponse) result).getRanking());
				listTopUsers.setAdapter(adapter);
			}
		}

		@Override
		public void onFailure(Object result) {
			AlertUtils.showAlertDialog(getSupportActivity(), getSupportActionBarContext()
					.getString(R.string.couldnt_load_ranking));

		}

		@Override
		public void onCancel() {
			AlertUtils.showAlertDialog(getSupportActivity(), getSupportActionBarContext()
					.getString(R.string.couldnt_load_ranking));
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.top_list, container, false);
		listTopUsers = (ListView) view.findViewById(R.id.listTopUsers);
		return view;
	}

	public void loadRanking() {
		if (WebServiceProvider.isNetworkAvailable(getSupportActivity())) {
			RankingAsyncTask task = new RankingAsyncTask(getSupportActivity(), asyncTaskListener);
			task.execute();
		} else {
			AlertUtils.showAlertDialog(getSupportActivity(), getSupportActionBarContext()
					.getString(R.string.internet_needed_to_saw_ranking));
		}
	}
}
