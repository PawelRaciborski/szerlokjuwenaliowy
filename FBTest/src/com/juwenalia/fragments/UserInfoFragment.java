package com.juwenalia.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.TextView;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.utilities.StorageUtils;

public class UserInfoFragment extends Fragment {

	private TextView txtUserPoints, txtUserPointsLeft, txtLogedUserName;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.user_info, container, false);
		txtUserPoints = (TextView) view.findViewById(R.id.txtUserPoints);
		txtUserPointsLeft = (TextView) view.findViewById(R.id.txtUserPointsLeft);
		txtLogedUserName = (TextView) view.findViewById(R.id.txtLogedUserName);
		String name = StorageUtils.getUserName(getSupportActivity());
		if (TextUtils.isEmpty(name)) {
			name = "<NO NAME>";
		}
		txtLogedUserName.setText(name);
		return view;
	}

	public void loadPoints() {
		DbAdapter adapter = new DbAdapter(getSupportActivity());
		adapter.open();
		txtUserPoints.setText("Punkty: " + adapter.getUserPoints());
		long tmp= adapter.getUserPoints()-StorageUtils.getUsedPoints(getSupportActivity());
		txtUserPointsLeft.setText("Pozosta�e: " + tmp);
		adapter.close();
	}

}
