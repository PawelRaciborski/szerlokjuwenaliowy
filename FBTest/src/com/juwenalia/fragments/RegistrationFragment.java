package com.juwenalia.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.activities.AwaitingActivity;
import com.juwenalia.activities.MainActivity;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.tasks.RegistrationAsyncTask;
import com.juwenalia.utilities.AlertUtils;
import com.juwenalia.utilities.Constants.UserTypes;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.utilities.ValitationUtils;
import com.juwenalia.ws.models.RegistrationLoginResponse;

public class RegistrationFragment extends Fragment implements OnClickListener {

	private EditText username, email, password1, password2;
	private Button btnRegister;

	private AsyncTaskListener taskListener = new AsyncTaskListener() {
		@Override
		public void onSuccess(final Object result) {
			if (result != null && result instanceof RegistrationLoginResponse) {

				StorageUtils.setUserId(getSupportActivity(),
						((RegistrationLoginResponse) result).getUserId());
				StorageUtils.setUserName(getSupportActivity(),
						((RegistrationLoginResponse) result).getUsername());
				StorageUtils.setUserActivated(getSupportActivity(),
						((RegistrationLoginResponse) result).getActivated());
				StorageUtils.setUserType(getSupportActivity(),
						((RegistrationLoginResponse) result).getUserType());
				AlertUtils.showAlertDialog(getSupportActivity(), getSupportActionBarContext()
						.getString(R.string.register_succesful),
						getSupportActivity().getString(R.string.ok),
						new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

								Intent intent;

								if (((RegistrationLoginResponse) result).getActivated()) {
									intent = new Intent(getSupportActivity(), MainActivity.class);
								} else {
									intent = new Intent(getSupportActivity(),
											AwaitingActivity.class);
								}
								getSupportActivity().startActivity(intent);
								getSupportActivity().finish();
							}
						});
			}
		}

		@Override
		public void onFailure(Object result) {
			AlertUtils.showAlertDialog(getSupportActivity(), result.toString());
		}

		@Override
		public void onCancel() {
			System.out.print("ccc");

		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.registration, container, false);
		username = (EditText) view.findViewById(R.id.editUserName);
		email = (EditText) view.findViewById(R.id.editEmail);
		password1 = (EditText) view.findViewById(R.id.editPassword1);
		password2 = (EditText) view.findViewById(R.id.editPassword2);
		btnRegister = (Button) view.findViewById(R.id.btnRegister);
		btnRegister.setOnClickListener(this);
		return view;
	}

	private boolean validateForm() {
		clearErrors();

		if (!ValitationUtils.validateUserName(username.getText().toString())) {
			username.requestFocus();
			username.setError(getString(R.string.field_required));
			return false;
		}

		if (!ValitationUtils.validateEmail(email.getText().toString())) {
			email.requestFocus();
			email.setError(getString(R.string.email_required));
			return false;
		}
		if (TextUtils.isEmpty(password1.getText().toString())) {
			password1.requestFocus();
			password1.setError(getString(R.string.field_required));
			return false;
		}
		if (TextUtils.isEmpty(password2.getText().toString())) {
			password2.requestFocus();
			password2.setError(getString(R.string.field_required));
			return false;
		}
		if (!password1.getText().toString().equals(password2.getText().toString())) {
			password2.requestFocus();
			password2.setError(getString(R.string.password_needs_to_be_identical));
			return false;
		} else if (!ValitationUtils.validatePasswords(password1.getText().toString())) {
			password1.requestFocus();
			password1.setError(String.format(getString(R.string.password_length_format),
					ValitationUtils.MIN_PASSWORD_LENGTH, ValitationUtils.MAX_PASSWORD_LENGTH));
			return false;
		}

		return true;
	}

	public void clearErrors() {
		username.setError(null);
		email.setError(null);
		password1.setError(null);
		password2.setError(null);
	}

	@Override
	public void onClick(View v) {
		if (v == btnRegister) {
			if (validateForm()) {
				RegistrationAsyncTask task = new RegistrationAsyncTask(getSupportActivity(),
						taskListener, username.getText().toString(), email.getText().toString(),
						password1.getText().toString(), null, UserTypes.NORMAL);
				task.execute();
			}
		}

	}
}
