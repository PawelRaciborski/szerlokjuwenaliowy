package com.juwenalia.fragments;

import java.util.Arrays;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.juwenalia.R;
import com.juwenalia.activities.AwaitingActivity;
import com.juwenalia.activities.LoginActivity;
import com.juwenalia.activities.MainActivity;
import com.juwenalia.activities.SplashActivity;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.listeners.SwitchFragmentListener;
import com.juwenalia.tasks.NormalLoginAsyncTask;
import com.juwenalia.tasks.RegistrationAsyncTask;
import com.juwenalia.utilities.AlertUtils;
import com.juwenalia.utilities.Constants.UserTypes;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.utilities.ValitationUtils;
import com.juwenalia.ws.models.RegistrationLoginResponse;

public class LoginFragment extends Fragment implements OnClickListener {

	private static final String TAG = "MainFragment";
	private UiLifecycleHelper uiHelper;
	private SwitchFragmentListener switchListener;
	private Button btnRegister;
	private Button btnLoginNormal;
	private EditText txtLoginEmail, txtLoginPassword;

	private AsyncTaskListener taskListener = new AsyncTaskListener() {
		@Override
		public void onSuccess(Object result) {
			if (result != null && result instanceof RegistrationLoginResponse) {
				StorageUtils.setUserId(getSupportActivity(),
						((RegistrationLoginResponse) result).getUserId());
				StorageUtils.setUserName(getSupportActivity(),
						((RegistrationLoginResponse) result).getUsername());
				StorageUtils.setUserActivated(getSupportActivity(),
						((RegistrationLoginResponse) result).getActivated());
				StorageUtils.setUserType(getSupportActivity(),
						((RegistrationLoginResponse) result).getUserType());
				Intent intent;
				if (((RegistrationLoginResponse) result).getActivated()) {
					intent = new Intent(getSupportActivity(), SplashActivity.class);
				} else {
					intent = new Intent(getSupportActivity(), AwaitingActivity.class);
				}
				getSupportActivity().startActivity(intent);
				getSupportActivity().finish();
			}
		}

		@Override
		public void onFailure(Object result) {
			AlertUtils.showAlertDialog(getSupportActivity(), result.toString());
			logoutFacebook();
		}

		@Override
		public void onCancel() {
			logoutFacebook();

		}
	};

	public void logoutFacebook() {
		Session session = Session.getActiveSession();
		if (session != null) {
			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.login_fb, container, false);
		LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
		authButton.setFragment(this);
		authButton.setReadPermissions(Arrays.asList("email"));
		btnRegister = (Button) view.findViewById(R.id.btnRegisterSw);
		btnRegister.setOnClickListener(this);
		btnLoginNormal = (Button) view.findViewById(R.id.btnLoginNormal);
		btnLoginNormal.setOnClickListener(this);
		txtLoginEmail = (EditText) view.findViewById(R.id.txtLoginEmail);
		txtLoginPassword = (EditText) view.findViewById(R.id.txtLoginPassword);

		return view;
	}

	public void setSwitchFragmentListener(SwitchFragmentListener listener) {
		this.switchListener = listener;
	}

	@Override
	public void onResume() {
		super.onResume();

		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}

		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getSupportActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	Request request;

	private void makeFacebookRequest(final Session session) {
		if (request == null) {
			request = Request.newMeRequest(session, new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {

					if (session == Session.getActiveSession()) {
						if (user != null) {

							RegistrationAsyncTask task = new RegistrationAsyncTask(
									getSupportActivity(), taskListener, user.getUsername(), user
											.getProperty("email").toString(), user.getUsername()
											+ user.getId() + user.getProperty("email").toString(),
									user.getId(), UserTypes.FACEBOOK);
							task.execute();
						}
					}
					if (response.getError() != null) {
						AlertUtils.showAlertDialog(getSupportActivity(),
								getString(R.string.cant_connect_with_facebook),
								getString(R.string.ok), null);
					}
					request = null;
				}
			});
			request.executeAsync();
		}
	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {

			if (TextUtils.isEmpty(StorageUtils.getUserId(getSupportActivity()))) {
				makeFacebookRequest(session);
			} else {
				Intent intent = new Intent(getSupportActivity(), MainActivity.class);
				getSupportActivity().startActivity(intent);
				getSupportActivity().finish();
			}
		} else if (state.isClosed()) {

		}
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private boolean validateForm() {
		if (!ValitationUtils.validateEmail(txtLoginEmail.getText().toString())) {
			txtLoginEmail.requestFocus();
			txtLoginEmail.setError(getString(R.string.email_required));
			return false;
		}
		if (TextUtils.isEmpty(txtLoginPassword.getText().toString())) {
			txtLoginPassword.requestFocus();
			txtLoginPassword.setError(getString(R.string.field_required));
			return false;
		} else if (!ValitationUtils.validatePasswords(txtLoginPassword.getText().toString())) {
			txtLoginPassword.requestFocus();
			txtLoginPassword.setError(String.format(getString(R.string.password_length_format),
					ValitationUtils.MIN_PASSWORD_LENGTH, ValitationUtils.MAX_PASSWORD_LENGTH));
			return false;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v == btnRegister) {
			if (switchListener != null) {
				switchListener.switchFragment(LoginActivity.REGISTER_FRAGMENT_ID);
			}
		} else if (v == btnLoginNormal) {
			if (validateForm()) {
				NormalLoginAsyncTask task = new NormalLoginAsyncTask(getSupportActivity(),
						taskListener, txtLoginEmail.getText().toString(), txtLoginPassword
								.getText().toString());
				task.execute();
			}
		}

	}
}
