package com.juwenalia.activities;

import java.util.LinkedList;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.juwenalia.R;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.tasks.AppStateAsyncTask;
import com.juwenalia.tasks.ProcessAppStateAsyncTask;
import com.juwenalia.utilities.AlertUtils;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.ws.WebServiceProvider;
import com.juwenalia.ws.models.AppStateResponse;
import com.juwenalia.ws.models.Clue;
import com.juwenalia.ws.models.Prize;

public class SplashActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		// setupApp();
	}

	private void setupApp() {
		if (WebServiceProvider.isNetworkAvailable(this)) {

			DbAdapter adapter = new DbAdapter(this);
			adapter.open();
			LinkedList<Clue> cl = adapter.getAllCluesList();
			adapter.close();
			LinkedList<Long> usedId = null;
			if (cl != null && cl.size() > 0) {
				usedId = new LinkedList<Long>();
				for (Clue c : cl) {
					if (!c.available) {
						usedId.add(c.id);
					}
				}
			}
			AppStateAsyncTask task = new AppStateAsyncTask(this, this.appStateTaskListener, usedId);
			task.execute();
		} else {
			loadingFinished();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		// DbAdapter adapter = DbAdapter.getInstance();
		// adapter.init(getApplicationContext());
		// adapter.open(null);
		setupApp();
	}

	@Override
	protected void onStop() {
		super.onStop();
		// DbAdapter.getInstance().close(null);
	}

	private AsyncTaskListener appStateTaskListener = new AsyncTaskListener() {

		@Override
		public void onSuccess(Object result) {
			if (result != null) {
				AppStateResponse res = (AppStateResponse) result;
				if (!TextUtils.isEmpty(res.getTimestamp())) {
					StorageUtils.setAppStateTimestamp(SplashActivity.this, res.getTimestamp());
					StorageUtils.setBaseImageUrl(SplashActivity.this, res.getBaseUrl());
					StorageUtils.setUsedPoints(SplashActivity.this, res.getUsedPoints());
					if ((res.getCodes() != null && res.getCodes().length > 0)
							|| (res.getPrizes() != null && res.getPrizes().length > 0)) {
						ProcessAppStateAsyncTask task = new ProcessAppStateAsyncTask(
								SplashActivity.this, processAppStateTaskListener, res.getPrizes(),
								res.getCodes());
						task.execute();
					} else {
						loadingFinished();
					}
				}
			}
		}

		@Override
		public void onFailure(Object result) {
			handleProblemWithServer();// AlertUtils.showAlertDialog(SplashActivity.this,
										// "PROBLEM");
		}

		@Override
		public void onCancel() {
			handleProblemWithServer();// AlertUtils.showAlertDialog(SplashActivity.this,
										// "PROBLEM");

		}
	};

	public void handleProblemWithServer() {
		DbAdapter adapter = new DbAdapter(this);
		adapter.open();
		try {
			LinkedList<Clue> clues = adapter.getAllCluesList();
			LinkedList<Prize> prizes = adapter.getAllPrizesList();
			if (clues != null && prizes != null && clues.size() > 0 && prizes.size() > 0) {
				loadingFinished();
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			AlertUtils.showAlertDialog(SplashActivity.this, "PROBLEM");
		}
		adapter.close();
	}

	private AsyncTaskListener processAppStateTaskListener = new AsyncTaskListener() {

		@Override
		public void onSuccess(Object result) {
			loadingFinished();
		}

		@Override
		public void onFailure(Object result) {
			AlertUtils.showAlertDialog(SplashActivity.this, "PROBLEM");

		}

		@Override
		public void onCancel() {
			AlertUtils.showAlertDialog(SplashActivity.this, "PROBLEM");

		}
	};

	private void loadingFinished() {
		long time = System.currentTimeMillis();
		StorageUtils.setStopTimestamp(this, time);
		Intent intent = new Intent(this, MainActivity.class);
		this.startActivity(intent);
		this.finish();
	}

}
