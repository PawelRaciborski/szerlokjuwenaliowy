package com.juwenalia.activities;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;

import com.juwenalia.R;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.tasks.UserActivatedAsyncTask;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.ws.models.ActivatedResponse;

public class AwaitingActivity extends BaseActivity {

	private static final int DELAY = 5000;
	private Timer timer;
	// private Handler handler;
	private AsyncTaskListener taskListener = new AsyncTaskListener() {

		@Override
		public void onSuccess(Object result) {
			if (result != null) {

				StorageUtils.setUserActivated(AwaitingActivity.this,
						((ActivatedResponse) result).getActivated());

				if (((ActivatedResponse) result).getActivated()) {
					Intent intent = new Intent(AwaitingActivity.this, SplashActivity.class);
					AwaitingActivity.this.startActivity(intent);
					AwaitingActivity.this.finish();
				} else {
					prepareTimer(DELAY);
				}
			}

		}

		@Override
		public void onFailure(Object result) {
			prepareTimer(DELAY);

		}

		@Override
		public void onCancel() {
			prepareTimer(DELAY);

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.awaiting);
		// handler = new Handler();
		// timer = new Timer();
		// timerTask = new CheckerTimerTask();
		// timer.schedule(timerTask, DELAY);
		prepareTimer(DELAY);
	}

	private void prepareTimer(long delay) {
		timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				AwaitingActivity.this.runOnUiThread(Timer_Tick);
				// prepareGetUserDetails(EnterVerificationCode.this,
				// PreferencesUtils.getCredentials(EnterVerificationCode.this));
			}
		}, delay);

	}

	private final Runnable Timer_Tick = new Runnable() {
		@Override
		public void run() {
			try {

				UserActivatedAsyncTask performBackgroundTask = new UserActivatedAsyncTask(
						AwaitingActivity.this, taskListener,
						StorageUtils.getUserId(AwaitingActivity.this));
				performBackgroundTask.execute();
			} catch (Exception e) {
				System.out.print(e.getMessage());
			}
		}
	};

	// class CheckerTimerTask extends TimerTask {
	// private final Handler handler = new Handler();
	//
	// @Override
	// public void run() {
	// handler.post(new Runnable() {
	// public void run() {
	// try {
	//
	// UserActivatedAsyncTask performBackgroundTask = new
	// UserActivatedAsyncTask(
	// AwaitingActivity.this, taskListener, StorageUtils
	// .getUserId(AwaitingActivity.this));
	// performBackgroundTask.execute();
	// } catch (Exception e) {
	// System.out.print(e.getMessage());
	// }
	// }
	// });
	// }

	// }
}
