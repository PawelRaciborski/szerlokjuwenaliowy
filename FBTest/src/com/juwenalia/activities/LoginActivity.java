package com.juwenalia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;

import com.juwenalia.R;
import com.juwenalia.fragments.LoginFragment;
import com.juwenalia.fragments.RegistrationFragment;
import com.juwenalia.listeners.DbChangedListener;
import com.juwenalia.listeners.SwitchFragmentListener;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.utilities.StorageUtils;

public class LoginActivity extends BaseActivity implements SwitchFragmentListener,
		DbChangedListener {
	private LoginFragment fragmentFbLogin;
	private RegistrationFragment fragmentRegister;
	public static final int LOGIN_FRAGMENT_ID = 0, REGISTER_FRAGMENT_ID = 1;

	public static final String START_WITH_ACTION_LOGOUT = "START_WITH_ACTION_LOGOUT";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		View v = findViewById(R.id.parent);
		if (v != null) {
			setupUI(v);
		}

		FragmentManager fm = getSupportFragmentManager();
		fragmentFbLogin = (LoginFragment) fm.findFragmentById(R.id.fragmentFbLogin);
		fragmentFbLogin.setSwitchFragmentListener(this);
		fragmentRegister = (RegistrationFragment) fm.findFragmentById(R.id.fragmentRegister);

		Bundle bun = getIntent().getExtras();

		boolean isLogout = bun == null ? false : bun.getBoolean(START_WITH_ACTION_LOGOUT, false);

		String userId = StorageUtils.getUserId(this);
		switchFragment(LOGIN_FRAGMENT_ID);
		if (isLogout) {
			StorageUtils.clearStorage(this);
			fragmentFbLogin.logoutFacebook();
			DbAdapter adapter = new DbAdapter(this);
			adapter.open();
			adapter.clear();
			adapter.close();

		} else if (!TextUtils.isEmpty(userId)) {
			Intent intent;
			if (StorageUtils.getUserActivated(this)) {
				intent = new Intent(this, SplashActivity.class);
			} else {
				intent = new Intent(this, AwaitingActivity.class);
			}
			startActivity(intent);
			finish();
		}
		// else {
		// if (!StorageUtils.getMessageDisplayed(this)) {
		// AlertUtils.showAlertDialog(this,
		// getString(R.string.info_dialog_text));
		// StorageUtils.setMessageDisplayed(this, true);
		// }
		// }

	}

	@Override
	public void switchFragment(int fragmentId) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		if (fragmentId == LOGIN_FRAGMENT_ID) {
			transaction.show(fragmentFbLogin);
			transaction.hide(fragmentRegister);
		} else {

			transaction.show(fragmentRegister);
			transaction.hide(fragmentFbLogin);
			transaction.addToBackStack(null);
		}
		transaction.commit();
	}

	@Override
	public void dbChanged(DbTables table) {
		// TODO Auto-generated method stub

	}

}
