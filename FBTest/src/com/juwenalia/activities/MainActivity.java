package com.juwenalia.activities;

import java.util.ArrayList;
import java.util.LinkedList;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.DrawerLayout;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.TextView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.juwenalia.R;
import com.juwenalia.adapters.MenuListAdapter;
import com.juwenalia.fragments.CluesFragment;
import com.juwenalia.fragments.PrizesFragment;
import com.juwenalia.fragments.StartFragment;
import com.juwenalia.fragments.TopListFragment;
import com.juwenalia.fragments.UserInfoFragment;
import com.juwenalia.listeners.AsyncTaskListener;
import com.juwenalia.listeners.DbChangedListener;
import com.juwenalia.listeners.LocationChangedListener;
import com.juwenalia.listeners.SwitchFragmentListener;
import com.juwenalia.tasks.GrantPrizeAsyncTask;
import com.juwenalia.utilities.AlertUtils;
import com.juwenalia.utilities.Constants;
import com.juwenalia.utilities.Constants.UserTypes;
import com.juwenalia.utilities.DbAdapter;
import com.juwenalia.utilities.GeolocationHelper;
import com.juwenalia.utilities.QrCodeUtils;
import com.juwenalia.utilities.QrCodeUtils.CodeTypes;
import com.juwenalia.utilities.StorageUtils;
import com.juwenalia.ws.WebServiceProvider;
import com.juwenalia.ws.models.Prize;

public class MainActivity extends BaseActivity implements DbChangedListener {

	// //// Drawer ///////
	DrawerLayout mDrawerLayout;
	ListView mDrawerList;
	ActionBarDrawerToggle mDrawerToggle;
	MenuListAdapter mMenuAdapter;
	String[] title;
	String[] subtitle;
	int[] icon;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	// //// Drawer ///////
	private UserInfoFragment userInfoFragment;
	private static final int START = 0;
	private static final int PRIZES = 1;
	private static final int CLUES = 2;
	private static final int TOP_LIST = 3;
	private static final int RULES = 4;
	private static final int ABOUT = 5;
	private static final int FRAGMENT_COUNT = 6;
	private static final String SAVE_CUSTOM_BACKSTACK = "SAVE_CUSTOM_BACKSTACK";

	private Fragment[] fragments = new Fragment[FRAGMENT_COUNT];

	private LinkedList<Integer> customBackStack;

	private GeolocationHelper locationHelper;
	private SwitchFragmentListener startSwitchFragmentListener = new SwitchFragmentListener() {

		@Override
		public void switchFragment(int fragmentId) {
			switch (fragmentId) {
			case StartFragment.SCAN:
				try {
					// Add extra five minutes before restart

					long time = StorageUtils.getStopTimestamp(MainActivity.this);
					long currentTime = System.currentTimeMillis();
					long diff = currentTime - time;
					if (diff > Constants.REFRESH_INTERVAL_MS - Constants.REFRESH_MARGIN_MS) {
						time += Constants.REFRESH_ADDITIONAL_MS;
						StorageUtils.setStopTimestamp(MainActivity.this, time);
					}
					Intent intent = new Intent("com.google.zxing.client.android.SCAN");
					intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
					startActivityForResult(intent, 0);

				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case StartFragment.PRIZES:

				showFragment(PRIZES, true);
				break;
			case StartFragment.CLUES:
				if (locationHelper.getBestLocation() == null) {
					AlertUtils.showAlertDialog(MainActivity.this,
							MainActivity.this.getString(R.string.location_info_dialog));
				}
				((CluesFragment) fragments[CLUES]).loadClues(locationHelper.getBestLocation());
				showFragment(CLUES, true);
				break;
			case StartFragment.TOP:
				((TopListFragment) fragments[TOP_LIST]).loadRanking();
				showFragment(TOP_LIST, true);
				break;
			default:
				break;
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		// if (!StorageUtils.getMessageDisplayed(this)) {
		// AlertUtils.showAlertDialog(this,
		// getString(R.string.info_dialog_text));
		// StorageUtils.setMessageDisplayed(this, true);
		// }
		locationHelper = new GeolocationHelper(this);

		FragmentManager fm = getSupportFragmentManager();
		userInfoFragment = (UserInfoFragment) fm.findFragmentById(R.id.userInfoFragment);
		fragments[START] = (Fragment) fm.findFragmentById(R.id.startFragment);
		((StartFragment) fragments[START]).setSwitchFragmentListener(startSwitchFragmentListener);
		fragments[PRIZES] = (Fragment) fm.findFragmentById(R.id.prizesFragment);
		fragments[CLUES] = (Fragment) fm.findFragmentById(R.id.cluesFragment);
		fragments[TOP_LIST] = (Fragment) fm.findFragmentById(R.id.topListFragment);
		fragments[RULES] = (Fragment) fm.findFragmentById(R.id.rulesFragment);
		fragments[ABOUT] = (Fragment) fm.findFragmentById(R.id.aboutFragment);

		FragmentTransaction transaction = fm.beginTransaction();
		for (int i = 0; i < fragments.length; i++) {
			transaction.hide(fragments[i]);
		}
		transaction.commit();

		mTitle = mDrawerTitle = getTitle();

		title = getResources().getStringArray(R.array.side_menu_items);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mDrawerList = (ListView) findViewById(R.id.listview_drawer);

		mMenuAdapter = new MenuListAdapter(MainActivity.this, title);

		mDrawerList.setAdapter(mMenuAdapter);

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
				R.string.drawer_open, R.string.drawer_close) {

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(mDrawerTitle);
				super.onDrawerOpened(drawerView);
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState != null) {
			ArrayList<Integer> tmp = savedInstanceState.getIntegerArrayList(SAVE_CUSTOM_BACKSTACK);

			if (tmp != null && tmp.size() > 0) {
				customBackStack = new LinkedList<Integer>(tmp);
				showFragment(customBackStack.getLast(), false);
			}
		} else {
			showFragment(START, true);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (customBackStack != null) {
			outState.putIntegerArrayList(SAVE_CUSTOM_BACKSTACK, new ArrayList<Integer>(
					customBackStack));
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		long time = System.currentTimeMillis();
		long lastTime = StorageUtils.getStopTimestamp(this);
		long diff = time - lastTime;
		// Reset app if not refreshed for an hour
		// if (lastTime > 0 && diff > 3600000) {
		if (lastTime > 0 && diff > Constants.REFRESH_INTERVAL_MS) {
			Intent intent = new Intent(this, SplashActivity.class);
			this.startActivity(intent);
			this.finish();
		}

		AlertUtils.showToast(this, getString(R.string.waiting_for_location));
		locationHelper.addListeners();
		userInfoFragment.loadPoints();
		((PrizesFragment) fragments[PRIZES]).loadPrizes();
		// ((CluesFragment)
		// fragments[CLUES]).loadClues(locationHelper.getBestLocation());
		locationHelper.setListener(new LocationChangedListener() {
			@Override
			public void onLocationChanged(Location location, boolean quality) {
				AlertUtils.showToast(MainActivity.this,
						MainActivity.this.getString(R.string.location_available_toast)
								+ (quality ? MainActivity.this.getString(R.string.high_accuracy)
										: MainActivity.this.getString(R.string.low_accuracy)));
				((CluesFragment) fragments[CLUES]).loadClues(locationHelper.getBestLocation());
			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
		locationHelper.removeListeners();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == android.R.id.home) {

			if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
				mDrawerLayout.closeDrawer(GravityCompat.START);
			} else {
				mDrawerLayout.openDrawer(GravityCompat.START);
			}
			return true;
		}

		return false;
	}

	private void showFragment(int fragmentIndex, boolean addToBackStack) {
		FragmentManager fm = getSupportFragmentManager();
		setTitle(title[fragmentIndex]);
		FragmentTransaction transaction = fm.beginTransaction();
		for (int i = 0; i < fragments.length; i++) {
			if (i == fragmentIndex) {
				transaction.show(fragments[i]);
			} else {
				transaction.hide(fragments[i]);
			}
		}
		if (addToBackStack) {
			if (customBackStack == null) {
				customBackStack = new LinkedList<Integer>();
			} else {
				while (customBackStack.size() > 50) {
					customBackStack.removeFirst();
				}
			}
			customBackStack.addLast(fragmentIndex);
		}
		transaction.commit();
	}

	private class DrawerItemClickListener implements ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parrent, View v, int position, long id) {
			selectItem(position);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	private void selectItem(int position) {
		if (position == 0) {
			showFragment(START, true);
		} else if (position == 1) {
			if (StorageUtils.getUserType(this) == UserTypes.GAME) {
				AlertUtils.showAlertDialog(this, "Opcja niedostępna w trakcie gry miejskiej!");
			} else {
				showFragment(PRIZES, true);
			}
		} else if (position == 2) {
			((CluesFragment) fragments[CLUES]).loadClues(locationHelper.getBestLocation());
			showFragment(CLUES, true);
			if (locationHelper.getBestLocation() == null) {
				AlertUtils.showAlertDialog(MainActivity.this,
						MainActivity.this.getString(R.string.location_info_dialog));
			}
		} else if (position == 3) {
			((TopListFragment) fragments[TOP_LIST]).loadRanking();
			showFragment(TOP_LIST, true);
		} else if (position == 4) {
			String url = StorageUtils.getUserType(this) == UserTypes.GAME ? Constants.GAME_RULES
					: Constants.SZERLOK_RULES;
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);
		} else if (position == 5) {
			showFragment(ABOUT, true);
		} else if (position == 6) {
			AlertUtils.produceAlertDialog(this, getString(R.string.logout_question), null,
					getString(R.string.yes), getString(R.string.no), null,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(MainActivity.this, LoginActivity.class);
							intent.putExtra(LoginActivity.START_WITH_ACTION_LOGOUT, true);
							startActivity(intent);
							finish();
						}
					}, null, null, true).show();
		}
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0) {
			if (resultCode == Activity.RESULT_OK) {
				String result = data.getStringExtra("SCAN_RESULT");
				CodeTypes type = QrCodeUtils.detectCodeType(result);

				switch (type) {
				case NormalCode:
					QrCodeUtils.processNormalCode(this, result, locationHelper.getBestLocation());
					((CluesFragment) fragments[CLUES]).loadClues(locationHelper.getBestLocation());
					break;
				case PrizeCode:
					Prize p = QrCodeUtils.processPrizeCode(this, result);
					displayPrize(p);
					break;
				case InfoCode:
					QrCodeUtils.processInfoCode(this, result);
					break;
				default:
					AlertUtils.showAlertDialog(this, "Niewłaściwy kod");
					break;
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				AlertUtils.showAlertDialog(this, getString(R.string.scan_canceled));
			}
		}
	}

	@Override
	public void onBackPressed() {
		if (customBackStack.size() > 1) {
			customBackStack.removeLast();
			int prev = customBackStack.getLast();
			showFragment(prev, false);
		} else {
			super.onBackPressed();
		}
	}

	public void displayPrize(final Prize prize) {
		if (prize != null) {
			DbAdapter adapter = new DbAdapter(this);
			adapter.open();
			long tmp = adapter.getUserPoints() - StorageUtils.getUsedPoints(this);
			adapter.close();
			if (tmp >= prize.points) {
				View v = AlertUtils.viewFromLayout(this, R.layout.collect_prize);
				TextView title = (TextView) v.findViewById(R.id.txtPrizeTitle);
				ImageView image = (ImageView) v.findViewById(R.id.imgPrize);
				title.setText(prize.name);
				Bitmap img = prize.getPrizeBitmap();
				if (img != null) {
					image.setImageBitmap(img);
				}
				AlertDialog alert = AlertUtils.produceAlertDialogWithCustomView(this,
						getString(R.string.would_you_like_to_recive_this_prize),
						getString(R.string.yes), getString(R.string.no), null,
						// Positive click
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (!WebServiceProvider.isNetworkAvailable(MainActivity.this)) {
									AlertUtils.showAlertDialog(MainActivity.this,
											getString(R.string.internet_connection_needed));
								} else {
									GrantPrizeAsyncTask task = new GrantPrizeAsyncTask(
											MainActivity.this, new AsyncTaskListener() {
												@Override
												public void onSuccess(Object result) {
													AlertUtils
															.showAlertDialog(
																	MainActivity.this,
																	getString(R.string.user_earn_prize_info),
																	getString(R.string.ok),
																	new DialogInterface.OnClickListener() {
																		@Override
																		public void onClick(
																				DialogInterface arg0,
																				int arg1) {
																			Intent intent = new Intent(
																					MainActivity.this,
																					SplashActivity.class);
																			MainActivity.this
																					.startActivity(intent);
																			MainActivity.this
																					.finish();
																		}
																	});
												}

												@Override
												public void onFailure(Object result) {
													AlertUtils.showAlertDialog(MainActivity.this,
															getString(R.string.cant_grant_prize));
												}

												@Override
												public void onCancel() {
													AlertUtils.showAlertDialog(MainActivity.this,
															getString(R.string.operation_canceled));
												}
											}, prize.id);
									task.execute();
								}

							}
						}, null, null, true, v);

				alert.show();
			} else {
				AlertUtils.showAlertDialog(this, getString(R.string.not_enough_points));
			}
		} else {
			AlertUtils.showAlertDialog(this, getString(R.string.code_not_in_db));
		}
	}

	@Override
	public void dbChanged(DbTables table) {
		if (table == DbTables.Prizes || table == DbTables.Both) {
			((PrizesFragment) fragments[PRIZES]).loadPrizes();
		}
		if (table == DbTables.Clues || table == DbTables.Both) {
			userInfoFragment.loadPoints();
			((CluesFragment) fragments[CLUES]).loadClues(locationHelper.getBestLocation());
		}
	}

}
