package com.juwenalia.adapters;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.juwenalia.R;
import com.juwenalia.ws.models.Prize;

public class PrizesAdapter extends ArrayAdapter<Prize> {
	private Context context;
	private Prize[] values;

	public PrizesAdapter(Context context, int textViewResourceId, Prize[] objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.values = objects;
	}

	private class ViewHolder {
		TextView txtName;
		TextView txtDescription;
		TextView txtAddress;
		TextView txtPoints;
		ImageView imgPrizeImage;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = vi.inflate(R.layout.prize_item, null);
		holder = new ViewHolder();
		holder.txtName = (TextView) convertView.findViewById(R.id.txtPrizeName);
		holder.txtDescription = (TextView) convertView.findViewById(R.id.txtPrizeDescription);
		holder.txtAddress = (TextView) convertView.findViewById(R.id.txtPrizeAddress);
		holder.txtPoints = (TextView) convertView.findViewById(R.id.txtPrizePoinsts);
		holder.imgPrizeImage = (ImageView) convertView.findViewById(R.id.imgPrizeImage);

		convertView.setTag(holder);
		Prize selected = this.values[position];

		holder.txtName.setText(selected.name);
		holder.txtDescription.setText(selected.description);
		holder.txtAddress.setText(selected.address);
		holder.txtPoints.setText(context.getString(R.string.points_semi) + selected.points);
		Bitmap image = selected.getPrizeBitmap();
		if (selected.available) {
			if (image != null) {
				holder.imgPrizeImage.setImageBitmap(image);
			} else {
				// TODO: correct placeholder
				holder.imgPrizeImage.setImageResource(R.drawable.juw_logo);
			}
		} else {
			holder.imgPrizeImage.setImageResource(R.drawable.szerlok_disabled_icon);
		}

		return convertView;
	}

}
