package com.juwenalia.adapters;

import java.util.Arrays;
import java.util.Comparator;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.location.Location;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.juwenalia.R;
import com.juwenalia.ws.models.Clue;

public class ClueAdapter extends ArrayAdapter<Clue> {

	private Clue[] values;
	private Context context;
	private Location location;

	public ClueAdapter(Context context, int textViewResourceId, Clue[] objects, Location location) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.values = objects;
		this.location = location;

		Arrays.sort(this.values, new ClueComparator());
	}

	// public static Comparator<Clue> clueItemComparator = new
	// Comparator<Clue>() {
	//
	// public int compare(Clue it1, Clue it2) {
	//
	// // if (ClueAdapter.this.location != null) {
	//
	// float dist1 = it1.getDistance(ClueAdapter.this.location);
	// float dist2 = it2.getDistance(ClueAdapter.this.location);
	// if (dist1 > dist2) {
	// return -1;
	// } else if (dist1 < dist2) {
	// return 1;
	// } else {
	// return 0;
	// }
	// // } else {
	// // return 0;
	// // }
	//
	// }
	//
	// };

	private class ClueComparator implements Comparator<Clue> {
		public int compare(Clue it1, Clue it2) {

			if (ClueAdapter.this.location != null) {

				float dist1 = it1.getDistance(ClueAdapter.this.location);
				float dist2 = it2.getDistance(ClueAdapter.this.location);
				if (dist1 > dist2) {
					return 1;
				} else if (dist1 < dist2) {
					return -1;
				} else {
					return 0;
				}
			} else {
				return 0;
			}

		}
	}

	private class ViewHolder {
		TextView txtDescription;
		TextView txtDistance;
		ImageView imgClueUsed;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = vi.inflate(R.layout.clue_item, null);
		holder = new ViewHolder();
		holder.txtDescription = (TextView) convertView.findViewById(R.id.txtClueDescription);
		holder.txtDistance = (TextView) convertView.findViewById(R.id.txtClueDistance);
		holder.imgClueUsed = (ImageView) convertView.findViewById(R.id.imgClueUsed);

		convertView.setTag(holder);
		Clue selected = this.values[position];
		holder.txtDescription.setText(selected.description);
		selected.getDistance(location);
		holder.imgClueUsed.setVisibility(selected.available ? View.GONE : View.VISIBLE);
		holder.txtDistance.setText(context.getString(R.string.distence)
				+ (location != null ? selected.getIntDistance(location) : context
						.getString(R.string.unknown_distance)) + " m");

		return convertView;
	}
}
