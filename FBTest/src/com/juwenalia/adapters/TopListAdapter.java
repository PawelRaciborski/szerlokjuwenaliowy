package com.juwenalia.adapters;

import java.util.Arrays;
import java.util.Comparator;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.juwenalia.R;
import com.juwenalia.ws.models.TopListItem;

public class TopListAdapter extends ArrayAdapter<TopListItem> {

	private TopListItem[] values;
	private Context context;

	public TopListAdapter(Context context, int textViewResourceId, TopListItem[] objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.values = objects;
		Arrays.sort(this.values, topListItemComparator);
	}

	public static Comparator<TopListItem> topListItemComparator = new Comparator<TopListItem>() {

		public int compare(TopListItem it1, TopListItem it2) {

			if (it1.points > it2.points) {
				return -1;
			} else if (it1.points < it2.points) {
				return 1;
			} else {
				return 0;
			}

		}

	};

	private class ViewHolder {
		TextView txtPosition;
		TextView txtUsername;
		TextView txtPoints;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = vi.inflate(R.layout.top_item, null);
		holder = new ViewHolder();
		holder.txtPosition = (TextView) convertView.findViewById(R.id.txtUserPosition);
		holder.txtUsername = (TextView) convertView.findViewById(R.id.txtUserName);
		holder.txtPoints = (TextView) convertView.findViewById(R.id.txtUserPoints);
		convertView.setTag(holder);

		TopListItem selected = this.values[position];

		holder.txtPosition.setText(Integer.toString(position + 1));
		holder.txtUsername.setText(selected.username);
		holder.txtPoints.setText(context.getString(R.string.points_semi) + selected.points);

		return convertView;
	}

}
